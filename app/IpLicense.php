<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Relations\Pivot;

class IpLicense extends Model
{
    use SoftDeletes;
    //make sure use SoftDeletes is first/topmost here
    //so it can be used for the whole model

     protected $fillable = [
    			
                "code",
                "ip_asset_id",
                "duration_in_months",
                "ip_license_status_id"
    ];

    //laravel relationship one-to-many (inverse)
	public function ip_asset()
	{
		return $this->belongsTo('App\IpAssets');
	}


    //laravel relationship one-to-many (inverse)
    public function ip_license_status()
    {
        return $this->belongsTo('App\IpLicenseStatus');
    }


    //Many to Many relationship 
    public function tickets(){

     return $this->belongsToMany('App\Ticket')
         ->withPivot('duration_in_months', 'date_needed', 'return_date', 'ip_license_id')
         ->withTimestamps();
    }
}
