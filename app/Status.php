<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = ["name"];


    //laravel relationship one-to-many
    public function ip_assets()
    {
    	return $this->hasMany('App\IpAssets');
    }
}
