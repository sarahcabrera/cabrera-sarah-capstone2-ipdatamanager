<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketStatus extends Model
{
    

    protected $fillable = ["name"];

    //laravel relationship one-to-many
    public function tickets()
    {
    	return $this->hasMany('App\Ticket');
    }
}
