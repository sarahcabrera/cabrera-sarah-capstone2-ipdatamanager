<?php

namespace App\Http\Controllers;

use App\Providers\AppServiceProvider;

use App\LicenseRequest;
use Illuminate\Http\Request;
use App\Status;
use App\IpAssets;
use App\IpLicense;
use App\IpLicenseStatus;
use Session;
use Carbon\Carbon;
use Auth;

class LicenseRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        //dd(session('license_request'));

        $ip_assets = IpAssets::all();

        $ip_license_statuses = IpLicenseStatus::all();

        $statuses = Status::all();

        if (session()->has('license_request')){


            $ip_licenses = IpLicense::find(array_keys(session('license_request')) );

           // dd($ip_licenses);
           
            
            foreach ($ip_licenses as $ip_license) {


                $ip_license->duration_in_months = session("license_request.$ip_license->id.duration_in_months"); 
                                                   

                //dd($ip_license->duration_in_months);


                $ip_license->date_needed = session("license_request.$ip_license->id.date_needed");

                

                $ip_license->return_date = session("license_request.$ip_license->id.return_date");

                
            }


            return view('license_requests.index')
                    ->with('ip_licenses', $ip_licenses)
                    ->with('ip_assets', $ip_assets)
                    ->with('statuses', $statuses)
                    ->with('ip_license_statuses', $ip_license_statuses);
                    

        } else {

            return view('license_requests.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LicenseRequest  $licenseRequest
     * @return \Illuminate\Http\Response
     */
    public function show(LicenseRequest $licenseRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LicenseRequest  $licenseRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(LicenseRequest $licenseRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LicenseRequest  $licenseRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //dd($request);

        $validatedData = $request->validate([

            'set_duration' => 'integer',
            'duration_in_months' => 'required|integer',
            'date_needed' => 'required|date',
            'return_date' => 'required|date|after:date_needed',

        ]);


        if($request->duration_in_months > $request->set_duration){

            return redirect()->back()
                ->with('message', "Request duration must be less than or equal to set duration.");

        } else {

            $date_needed = Carbon::parse($request->date_needed);
            $return_date = Carbon::parse($request->return_date);

            $diff =  $date_needed->diffInDays($return_date);

            $months = $diff/30;

            $duration_in_months = $request->duration_in_months;


            if($months == $duration_in_months || $months < $duration_in_months ){

                $request->session()->put("license_request.$id", $validatedData);

                return redirect( route('license_request.index') );

            } else {

                 return redirect()->back()
                        ->with('message', "Date range in months must be less than or equal to requested license duration.");

            }
        }
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LicenseRequest  $licenseRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        session()->forget("license_request.$id");

        //check if session license_request is empty
        if(count(session()->get('license_request')) === 0){

            session()->forget('license_request');
        }


        return redirect( route('license_request.index') );
    }

     function clear(){

        session()->forget('license_request');
        return redirect( route('license_request.index') );
    }
}
