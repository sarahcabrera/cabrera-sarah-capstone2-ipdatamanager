<?php

namespace App\Http\Controllers;

use App\IpLicenseStatus;
use Illuminate\Http\Request;

class IpLicenseStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IpLicenseStatus  $ipLicenseStatus
     * @return \Illuminate\Http\Response
     */
    public function show(IpLicenseStatus $ipLicenseStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IpLicenseStatus  $ipLicenseStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(IpLicenseStatus $ipLicenseStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IpLicenseStatus  $ipLicenseStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IpLicenseStatus $ipLicenseStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IpLicenseStatus  $ipLicenseStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(IpLicenseStatus $ipLicenseStatus)
    {
        //
    }
}
