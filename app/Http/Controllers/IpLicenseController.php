<?php

namespace App\Http\Controllers;

use App\IpAssets;
use App\IpLicense;
use App\IpLicenseStatus;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class IpLicenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ip_assets = IpAssets::all();

        $ip_license_statuses = IpLicenseStatus::all();

        $ip_licenses = IpLicense::all();

        


      
        return view('ip_licenses.index', ['ip_licenses' => $ip_licenses])
                ->with('ip_assets', $ip_assets)
                ->with('ip_license_statuses', $ip_license_statuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();

       if($user->id == 1 ){

            $ip_assets = IpAssets::all();

            $ip_license_statuses = IpLicenseStatus::all();



            return view('ip_licenses.create')
                    ->with('ip_assets', $ip_assets) 
                    ->with('ip_license_statuses', $ip_license_statuses);

        }else{

                $ip_assets = IpAssets::all();

            $ip_license_statuses = IpLicenseStatus::all();

            $ip_licenses = IpLicense::all();

            


          
            return view('ip_licenses.index', ['ip_licenses' => $ip_licenses])
                    ->with('ip_assets', $ip_assets)
                    ->with('ip_license_statuses', $ip_license_statuses);

        }

         
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request); 

        $validatedData = $request->validate([

            'code' => 'required|unique:ip_licenses,code',
            'ip_asset_id' => 'required|integer',
            'duration_in_months' => 'required|integer',
            'ip_license_status_id' => 'required|integer'

        ]);

        
        $ip_license = new IpLicense($validatedData);

        $ip_license->save();

       
       /* UPDATE IP ASSET INVENTORY AUTOMATICALLY */

        $ip_asset = IpAssets::all()
                        ->where('id', $ip_license->ip_asset_id);

        //dd($ip_asset);

       
        $ip_licenses = IpLicense::all()
                       ->where('ip_asset_id', $ip_license->ip_asset_id);

        //dd($ip_licenses);

        $available_ip_licenses = IpLicense::all()
                                  ->where('ip_asset_id', $ip_license->ip_asset_id)
                                  ->where('ip_license_status_id', 1);
        //dd($available_ip_licenses);


        $granted_licenses = IpLicense::all()
                          ->where('ip_asset_id', $ip_license->ip_asset_id)
                          ->where('ip_license_status_id', 2);
        //dd($granted_licenses);



        $license_limit = count($ip_licenses);

        $quantity_licensed = count($granted_licenses);

        $inventory = count($available_ip_licenses);


        DB::table('ip_assets')
            ->where('id', $ip_license->ip_asset_id)
            ->update(

                [

                    'license_limit' => $license_limit,
                    'quantity_licensed' => $quantity_licensed,
                    'inventory' => $inventory


                 ]
        
            );

                

        return redirect( route('ip_licenses.show', $ip_license->id) )
            ->with('message', 'IP License is added successfully. IP Asset Inventory updated.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IpLicense  $ipLicense
     * @return \Illuminate\Http\Response
     */
    public function show(IpLicense $ip_license)
    {

         $ip_assets = IpAssets::all();

        

         $ip_license_statuses = IpLicenseStatus::all();


         


         return view('ip_licenses.show')
                ->with('ip_license', $ip_license )
                ->with('ip_assets', $ip_assets)
                ->with('ip_license_statuses', $ip_license_statuses);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IpLicense  $ipLicense
     * @return \Illuminate\Http\Response
     */
    public function edit(IpLicense $ip_license)
    {

         $user = Auth::user();

       if($user->id == 1 ){
            $ip_assets = IpAssets::all();

            $ip_license_statuses = IpLicenseStatus::all();


            

            return view('ip_licenses.edit')
                   ->with('ip_license', $ip_license )
                   ->with('ip_assets', $ip_assets)
                   ->with('ip_license_statuses', $ip_license_statuses);

        }else{

                $ip_assets = IpAssets::all();

            $ip_license_statuses = IpLicenseStatus::all();

            $ip_licenses = IpLicense::all();

            


          
            return view('ip_licenses.index', ['ip_licenses' => $ip_licenses])
                    ->with('ip_assets', $ip_assets)
                    ->with('ip_license_statuses', $ip_license_statuses);

        }

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IpLicense  $ipLicense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IpLicense $ip_license)
    {

        $validatedData = $request->validate([

            'ip_asset_id' => 'required|integer',
            'duration_in_months' => 'required|integer',
            'ip_license_status_id' => 'required|integer'

        ]);

        
        $ip_license->update($validatedData);

        $ip_license->save();



        /* UPDATE IP ASSET INVENTORY AUTOMATICALLY */

        $ip_asset = IpAssets::all()
                        ->where('id', $ip_license->ip_asset_id);

        //dd($ip_asset);

       
        $ip_licenses = IpLicense::all()
                       ->where('ip_asset_id', $ip_license->ip_asset_id);

        //dd($ip_licenses);

        $available_ip_licenses = IpLicense::all()
                                  ->where('ip_asset_id', $ip_license->ip_asset_id)
                                  ->where('ip_license_status_id', 1);
        //dd($available_ip_licenses);


        $granted_licenses = IpLicense::all()
                          ->where('ip_asset_id', $ip_license->ip_asset_id)
                          ->where('ip_license_status_id', 2);
        //dd($granted_licenses);



        $license_limit = count($ip_licenses);

        $quantity_licensed = count($granted_licenses);

        $inventory = count($available_ip_licenses);


        DB::table('ip_assets')
            ->where('id', $ip_license->ip_asset_id)
            ->update(

                [

                    'license_limit' => $license_limit,
                    'quantity_licensed' => $quantity_licensed,
                    'inventory' => $inventory


                 ]
        
            );






        return redirect( route('ip_licenses.show', $ip_license->id) )
            ->with('message', 'IP License is updated successfully. IP Asset Inventory updated.');


        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IpLicense  $ipLicense
     * @return \Illuminate\Http\Response
     */
    public function destroy(IpLicense $ip_license)
    {
        
        $user = Auth::user();

               if($user->id == 1 ){
                    $ip_license->delete();
                
                /* UPDATE IP ASSET INVENTORY AUTOMATICALLY */

                $ip_asset = IpAssets::all()
                                ->where('id', $ip_license->ip_asset_id);

                //dd($ip_asset);

               
                $ip_licenses = IpLicense::all()
                               ->where('ip_asset_id', $ip_license->ip_asset_id);

                //dd($ip_licenses);

                $available_ip_licenses = IpLicense::all()
                                          ->where('ip_asset_id', $ip_license->ip_asset_id)
                                          ->where('ip_license_status_id', 1);
                //dd($available_ip_licenses);


                $granted_licenses = IpLicense::all()
                                  ->where('ip_asset_id', $ip_license->ip_asset_id)
                                  ->where('ip_license_status_id', 2);
                //dd($granted_licenses);



                $license_limit = count($ip_licenses);

                $quantity_licensed = count($granted_licenses);

                $inventory = count($available_ip_licenses);


                DB::table('ip_assets')
                    ->where('id', $ip_license->ip_asset_id)
                    ->update(

                        [

                            'license_limit' => $license_limit,
                            'quantity_licensed' => $quantity_licensed,
                            'inventory' => $inventory


                         ]
                
                    );

               return redirect(route('ip_licenses.index'))
                   ->with('message', 'IP License deleted successfully. IP Asset Inventory updated.')
                   ->with('alert', 'danger');

        }else{

                $ip_assets = IpAssets::all();

            $ip_license_statuses = IpLicenseStatus::all();

            $ip_licenses = IpLicense::all();

            


          
            return view('ip_licenses.index', ['ip_licenses' => $ip_licenses])
                    ->with('ip_assets', $ip_assets)
                    ->with('ip_license_statuses', $ip_license_statuses);

        }
        
    }
}
