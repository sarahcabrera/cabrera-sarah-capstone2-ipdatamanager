<?php

namespace App\Http\Controllers;

use App\Ip;
use App\Type;
use App\Status;
use App\RenewalStatus;
use App\IpAssets;
use App\IpLicense;
use App\IpLicenseStatus;
use App\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Validator; 
use Illuminate\Support\Arr;
use Auth;
use Aws\Credentials;
use App\User;
use Illuminate\Support\Facades\DB;



class IpAssetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $ip_assets = IpAssets::all();
        $ip_licenses_statuses = IpLicenseStatus::all();


      
        //return view('ip_assets.index', ['ip_assets' => $ip_assets]);

        return view('ip_assets.index')
                    ->with('ip_assets', $ip_assets);
                    // ->with('ip_licenses', $ip_licenses)
                    // ->with('available_ip_licenses', $available_ip_licenses)
                    // ->with('granted_licenses', $granted_licenses);

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        $user = Auth::user();

       if($user->id == 1 ){

            return view('ip_assets.create')
                ->with('ips', Ip::all())
                ->with('types', Type::all()->sortBy('name'))
                ->with('statuses', Status::all())
                ->with('renewal_statuses', RenewalStatus::all());

        }else{

            $ip_assets = IpAssets::all();
            $ip_licenses_statuses = IpLicenseStatus::all();

                 return view('ip_assets.index')
                    ->with('ip_assets', $ip_assets);

        }
        
           
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request); 

        $validatedData = $request->validate([
            'name' => 'required|unique:ip_assets,name',
            'creator' => 'required',
            'code' => 'required|unique:ip_assets,code',
            'description' => 'required',
            'ip_id' => 'required',
            'type_id' => 'required',
            'status_id' => 'required',
            'license_limit' => 'integer',
            'quantity_licensed' => 'integer',
            'inventory' => 'integer',
            'image' => 'required|image|max:3000',

            'creation_date' => 'date',
            'registered_date' => 'date|after:created_date',
            'renewal_due_date' => 'date|after:registered_date',
            'renewal_status_id' => 'required'

        ]);

      
        //save the image after validation
        //$path = $request->file('image')->store('public/ip_assets');
        $path = $request->file('image')->store('images', 's3');

        //dd(Storage::url($path));

        //return url of saved image
        $url = Storage::disk('s3')->url($path);

        //dd($url);

        // create new product entry
        $ip_asset = new IpAssets($validatedData);


        //assign the value of the image
        $ip_asset->image = $url;

        $ip_asset->path = $url;

        // save the ip_asset
        $ip_asset->save();

        return redirect( route('ip_assets.show', $ip_asset->id) )
                ->with('message', 'IP Asset is added successfully.');
       
    }
        
    /**
     * Display the specified resource.
     *
     * @param  \App\IpAssets  $ipAssets
     * @return \Illuminate\Http\Response
     */
    public function show(IpAssets $ip_asset)
    {

        $renewal_statuses = RenewalStatus::all();

        $ip_licenses_statuses = IpLicenseStatus::all();

        
        $ip_licenses = IpLicense::all()
                       ->where('ip_asset_id', $ip_asset->id);

        $available_ip_licenses = IpLicense::all()
                                  ->where('ip_asset_id', $ip_asset->id)
                                  ->where('ip_license_status_id', 1);


        $granted_licenses = IpLicense::all()
                          ->where('ip_asset_id', $ip_asset->id)
                          ->where('ip_license_status_id', 2);


        $ip_asset->license_limit = count($ip_licenses);

        $ip_asset->quantity_licensed = count($granted_licenses);

        $ip_asset->inventory = count($available_ip_licenses);


        //get users granted licenses
        //$approved_tickets = Ticket::all()->where('status_id', 2);

        //dd($approved_tickets);
        //$users = User::all();

        //dd($approved_tickets);
        //dd($granted_users);


        return view('ip_assets.show')
                ->with('ip_asset', $ip_asset)
                ->with('renewal_statuses', $renewal_statuses)
                ->with('ip_licenses', $ip_licenses)
                ->with('available_ip_licenses', $available_ip_licenses)
                ->with('granted_licenses', $granted_licenses);
                // ->with('approved_tickets', $approved_tickets)
                // ->with('users', $users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IpAssets  $ipAssets
     * @return \Illuminate\Http\Response
     */
    public function edit(IpAssets $ip_asset)
    {

          $user = Auth::user();

       if($user->id == 1 ){

            $ip_licenses = IpLicense::all()
                       ->where('ip_asset_id', $ip_asset->id);

            $available_ip_licenses = IpLicense::all()
                      ->where('ip_asset_id', $ip_asset->id)
                      ->where('ip_license_status_id', 1);


            $granted_licenses = IpLicense::all()
                          ->where('ip_asset_id', $ip_asset->id)
                          ->where('ip_license_status_id', 2);


            return view('ip_assets.edit')
                ->with('ip_asset', $ip_asset)
                ->with('ips', Ip::all())
                ->with('types', Type::all()->sortBy('name'))
                ->with('statuses', Status::all())
                ->with('renewal_statuses', RenewalStatus::all())
                 ->with('ip_licenses', $ip_licenses)
                ->with('available_ip_licenses', $available_ip_licenses)
                ->with('granted_licenses', $granted_licenses);

        }else{

            $ip_assets = IpAssets::all();
            $ip_licenses_statuses = IpLicenseStatus::all();
            
                 return view('ip_assets.index')
                    ->with('ip_assets', $ip_assets);

        }

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IpAssets  $ipAssets
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IpAssets $ip_asset)
    {

        if($request->has('update_inventory_db')){

                //dd($request);

                $ip_licenses = IpLicense::all()
                               ->where('ip_asset_id', $ip_asset->id);

                $available_ip_licenses = IpLicense::all()
                              ->where('ip_asset_id', $ip_asset->id)
                              ->where('ip_license_status_id', 1);


                $granted_licenses = IpLicense::all()
                                  ->where('ip_asset_id', $ip_asset->id)
                                  ->where('ip_license_status_id', 2);


                $ip_asset->license_limit = count($ip_licenses);

                $ip_asset->quantity_licensed = count($granted_licenses);

                $ip_asset->inventory = count($available_ip_licenses);

                // save ip_asset
                $ip_asset->save();

                return redirect(route('ip_assets.show', $ip_asset->id))
                        ->with('message', 'IP Asset Inventory is updated sucessfully.')
                        ->with('alert', 'success');

        } else {


                $validatedData = $request->validate([

                    'name' => 'required',
                    'creator' => 'required',
                    'image' => 'nullable|image|max:3000',

                    'ip_id' => 'required',
                    'status_id' => 'required',
                    'type_id' => 'required',
                    'renewal_status_id' => 'required',

                    'description' => 'required',

                    'creation_date' => 'nullable|date',
                    'registered_date' => 'nullable|date|after:created_date',
                    'renewal_due_date' => 'nullable|date|after:registered_date'

                ]);


                // update product
                $ip_asset->update($validatedData);


                $ip_licenses = IpLicense::all()
                               ->where('ip_asset_id', $ip_asset->id);

                $available_ip_licenses = IpLicense::all()
                              ->where('ip_asset_id', $ip_asset->id)
                              ->where('ip_license_status_id', 1);


                $granted_licenses = IpLicense::all()
                                  ->where('ip_asset_id', $ip_asset->id)
                                  ->where('ip_license_status_id', 2);


                $ip_asset->license_limit = count($ip_licenses);

                $ip_asset->quantity_licensed = count($granted_licenses);

                $ip_asset->inventory = count($available_ip_licenses);


            
                //check if there's an image uploaded
                //if there's an image, save it and get url
                if($request->hasFile('image')) {

                    //save the image after validation
                    $path = $request->file('image')->store('images', 's3');

                    //return url of saved image
                    $url = Storage::disk('s3')->url($path);

                    //assign the value of the image
                    $ip_asset->image = $url;

                    $ip_asset->path = $url;

                }


                // save ip_asset
                $ip_asset->save();

                return redirect(route('ip_assets.show', $ip_asset->id))
                        ->with('message', 'IP Asset is updated sucessfully.')
                        ->with('alert', 'success');

         }

    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IpAssets  $ipAssets
     * @return \Illuminate\Http\Response
     */
    public function destroy(IpAssets $ip_asset)
    {
         $user = Auth::user();

       if($user->id == 1 ){

            $ip_licenses = IpLicense::all()->where('ip_asset_id', $ip_asset->id);

               if(count($ip_licenses) > 0 ){

                 return redirect(route('ip_assets.index'))
                        ->with('message', 'Cannot delete IP Asset with existing IP Licenses. Delete IP Licenses first.')
                        ->with('alert', 'danger');

               }else{


                   $ip_asset->delete();

                   return redirect(route('ip_assets.index'))
                       ->with('message', 'IP asset deleted successfully.')
                       ->with('alert', 'danger');


               }

        }else{

            $ip_assets = IpAssets::all();
            $ip_licenses_statuses = IpLicenseStatus::all();
            
                 return view('ip_assets.index')
                    ->with('ip_assets', $ip_assets);

        }
       
       

    }
}
