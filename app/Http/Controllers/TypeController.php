<?php

namespace App\Http\Controllers;

use App\Type;
use App\IpAssets;
use Illuminate\Http\Request;
use Auth;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = Type::all()->sortBy('name');

        return view('types.index')
                ->with('types', $types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
         $user = Auth::user();

       if($user->id == 1 ){

            return view('types.create');


        }else{

            $types = Type::all()->sortBy('name');

            return view('types.index')
                ->with('types', $types);

        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([

            'name' => 'required|unique:ips,name'
                    
        ]);

        $type = new Type($validatedData);
        $type->save();

        return redirect(route('types.index'))
                 ->with('message', "Type {$type->name} is added successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type)
    {
        return view('types.show')->with('type', $type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {

        $user = Auth::user();

       if($user->id == 1 ){

            return view('types.edit')->with('type', $type);

        }else{

            $types = Type::all()->sortBy('name');

            return view('types.index')
                ->with('types', $types);

        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        $validatedData = $request->validate([

            'name' => 'required|unique:ips,name'
                    
        ]);

        $type->update($validatedData);
        $type->save();

        return redirect(route('types.show', $type->id))
                 ->with('message', "Type is updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
         
        $user = Auth::user();

       if($user->id == 1 ){

            $ip_assets = IpAssets::all()->where('type_id', $type->id);

            if(count($ip_assets) > 0){

                return redirect(route('types.index'))
                    ->with('message', "Cannot delete Type with existing IP Assets. Delete IP Assets first.")
                    ->with('alert', 'info');

            }else{

                $type->delete();
                return redirect(route('types.index'))
                        ->with('message', "Type {$type->name} has been deleted.")
                        ->with('alert', 'warning');

            }

        }else{

            $types = Type::all()->sortBy('name');

            return view('types.index')
                ->with('types', $types);

        }

         
        
    }
}
