<?php

namespace App\Http\Controllers;

use App\Ip;
use App\IpAssets;
use Illuminate\Http\Request;
use Auth;
use App\User;

class IpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $ips = Ip::all();
        //put all in Ip in $ips

        // dd($ips);

        return view('ips.index')
                ->with('ips', $ips);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
       $user = Auth::user();

       if($user->id == 1 ){

         return view('ips.create');

        }else{

                $ips = Ip::all();

                return view('ips.index')
                    ->with('ips', $ips)
                    ->with('alert', 'Unauthorized to access page.');

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validatedData = $request->validate([

            'name' => 'required|unique:ips,name',
                      //unique to ips table, column name
            'term' => 'required',
            'protection_start' => 'required',
            'description' => 'required',
            'works_covered' => 'required'
        ]);

        $ip = new Ip($validatedData);
        $ip->save();

        return redirect(route('ips.index'))
                 ->with('message', "IP Category {$ip->name} is added successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ip  $ip
     * @return \Illuminate\Http\Response
     */
    public function show(Ip $ip)
    {
        //
        return view('ips.show')->with('ip', $ip);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ip  $ip
     * @return \Illuminate\Http\Response
     */
    public function edit(Ip $ip)
    {

        $user = Auth::user();

       if($user->id == 1 ){

            return view('ips.edit')->with('ip', $ip);

        }else{

                $ips = Ip::all();

                return view('ips.index')
                    ->with('ips', $ips)
                    ->with('alert', 'Unauthorized to access page.');

        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ip  $ip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ip $ip)
    {
        $validatedData = $request->validate([

            'name' => 'required|unique:ips,name',
                      //unique to ips table, column name
            'term' => 'required',
            'protection_start' => 'required',
            'description' => 'required',
            'works_covered' => 'required'
        ]);

        $ip->update($validatedData);
        $ip->save();

        return redirect(route('ips.show', $ip->id))
                 ->with('message', "IP Category is updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ip  $ip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ip $ip)
    {

        $user = Auth::user();

       if($user->id == 1 ){

            $ip_assets = IpAssets::all()->where('ip_id', $ip->id);

            if(count($ip_assets) > 0){

                return redirect(route('ips.index'))
                    ->with('message', "Cannot delete IP Category with existing IP Assets. Delete IP Assets first.")
                    ->with('alert', 'info');

            }else{

                $ip->delete();
                return redirect(route('ips.index'))
                        ->with('message', "IP Category {$ip->name} has been deleted.")
                        ->with('alert', 'warning');

            }


        }else{

            $ips = Ip::all();

                return view('ips.index')
                    ->with('ips', $ips)
                    ->with('alert', 'Unauthorized to access page.');

        }
        

    }
}
