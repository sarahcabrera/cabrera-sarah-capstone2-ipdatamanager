<?php

namespace App\Http\Controllers;


use App\Ticket;
use App\TicketStatus;
use App\IpAssets;
use App\IpLicense;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Auth;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        $users = User::all()->sortBy('name');

        $ticket_statuses = TicketStatus::all();

        $tickets = Ticket::all();


        if ( Auth::user()->can('isAdmin') ){

            $tickets = Ticket::all();

          

        }else{

            $tickets = Ticket::all()
                        ->where('user_id', Auth::user()->id);

        }


        return view('tickets.index')
                ->with('users', $users)
                ->with('tickets', $tickets)
                ->with('ticket_statuses', $ticket_statuses);
               

    }

    public function user(Request $request, $id)
    {


        $users = User::all();

        $ticket_statuses = TicketStatus::all();

        $tickets = Ticket::all()->where('user_id', $id);


        return view('tickets.index')
                ->with('users', $users)
                ->with('tickets', $tickets)
                ->with('ticket_statuses', $ticket_statuses);

    }


    public function pending(Request $request)
    {
        $users = User::all();

        $ticket_statuses = TicketStatus::all();


        if ( Auth::user()->can('isAdmin')){

            $tickets = Ticket::all()->where('status_id', 1);


        }else{

            $tickets = Ticket::all()
                        ->where('user_id', Auth::user()->id)
                        ->where('status_id', 1);

        }

       

        return view('tickets.index')
                ->with('users', $users)
                ->with('tickets', $tickets)
                ->with('ticket_statuses', $ticket_statuses);

    }

    public function approved(Request $request)
    {
        $users = User::all();

        $ticket_statuses = TicketStatus::all();


        if ( Auth::user()->can('isAdmin')){

            $tickets = Ticket::all()->where('status_id', 2);


        }else{

            $tickets = Ticket::all()
                        ->where('user_id', Auth::user()->id)
                        ->where('status_id', 2);

        }

        return view('tickets.index')
                ->with('users', $users)
                ->with('tickets', $tickets)
                ->with('ticket_statuses', $ticket_statuses);

    }

    public function declined(Request $request)
    {
        $users = User::all();

       $ticket_statuses = TicketStatus::all();


        if ( Auth::user()->can('isAdmin')){

            $tickets = Ticket::all()->where('status_id', 3);


        }else{

            $tickets = Ticket::all()
                        ->where('user_id', Auth::user()->id)
                        ->where('status_id', 3);

        }

        return view('tickets.index')
                ->with('users', $users)
                ->with('tickets', $tickets)
                ->with('ticket_statuses', $ticket_statuses);

    }

    public function completed(Request $request)
    {
        $users = User::all();

       $ticket_statuses = TicketStatus::all();


        if ( Auth::user()->can('isAdmin')){

            $tickets = Ticket::all()->where('status_id', 4);


        }else{

            $tickets = Ticket::all()
                        ->where('user_id', Auth::user()->id)
                        ->where('status_id', 4);

        }

        return view('tickets.index')
                ->with('users', $users)
                ->with('tickets', $tickets)
                ->with('ticket_statuses', $ticket_statuses);

    }


    public function recent(Request $request)
    {
        $users = User::all();
       $ticket_statuses = TicketStatus::all();


        if ( Auth::user()->can('isAdmin')){

            $tickets = Ticket::all()->sortDesc();


        }else{

            $tickets = Ticket::all()->sortDesc()
                        ->where('user_id', Auth::user()->id);
                        

        }

        return view('tickets.index')
                ->with('users', $users)
                ->with('tickets', $tickets)
                ->with('ticket_statuses', $ticket_statuses);

    }

   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(Auth::user()->id);


        //get the details needed
        $ticket = new Ticket;
        //transaction_code
        $ticket->ticket_code = strtoupper(Str::random(13));
        // user_id
        $ticket->user_id = Auth::user()->id;
        // save to database
        $ticket->save();

        /* Populate the Pivot Table */

        //1. query database to get the ip_licenses
            //cannot be from the license_request session
            //must be from the database in case of status change

            $ip_licenses = IpLicense::find(array_keys(session('license_request')));


        //2. compute the subtotal and total
            foreach ($ip_licenses as $ip_license) {

                $ip_license->duration_in_months = session("license_request.$ip_license->id.duration_in_months"); 
                                                   
                //dd($ip_license->duration_in_months);

                $ip_license->date_needed = session("license_request.$ip_license->id.date_needed");

                $ip_license->return_date = session("license_request.$ip_license->id.return_date");

                //3. query to database (ip_license_tickets)
                $ticket->ip_licenses()->attach($ip_license->id, [

                    'duration_in_months' => $ip_license->duration_in_months,
                    'date_needed' => $ip_license->date_needed,
                    'return_date' => $ip_license->return_date
                ]);



                //dd($ip_license->id);


               /* Upon Request Checkout, Change License Request Status to Requested*/

                /*DB::table('ip_licenses')
                     ->where('id', $ip_license->id)
                     ->update(['request_status_id' => 2]);*/
            }

        //3. save transaction with details again
        $ticket->save();

        //4. clear cart
        session()->forget('license_request');


        return redirect(route('tickets.show', $ticket->id)); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {


        if ( Auth::user()->can('isAdmin') ){

            $tickets = Ticket::all();

            //dd($ticket);

            $ticket_statuses = TicketStatus::all();
            $ip_licenses = IpLicense::all();
            $ip_assets = IpAssets::all();

          

        }else if (Auth::user()->id === $ticket->user_id) {

            $tickets = Ticket::all();

            $ticket_statuses = TicketStatus::all();
            $ip_licenses = IpLicense::all();
            $ip_assets = IpAssets::all();

        } else {

            $users = User::all()->sortBy('name');

            $ticket_statuses = TicketStatus::all();

            $tickets = Ticket::all()
                          ->where('user_id', Auth::user()->id);


            return view('tickets.index')
                    ->with('users', $users)
                    ->with('tickets', $tickets)
                    ->with('ticket_statuses', $ticket_statuses)
                    ->with('message', "Unauthorized access.");
        }



        return view('tickets.show')
                ->with('ticket', $ticket)
                ->with('ticket_statuses', $ticket_statuses)
                ->with('ip_licenses', $ip_licenses)
                ->with('ip_assets', $ip_assets);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {

        $ticket_id = $ticket->id;

        return view('tickets.edit')
                ->with('ticket', $ticket)
                ->with('ticket_id', $ticket_id)
                ->with('ip_license_tickets', $ip_license_tickets);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {


        $ticket_id = $ticket->id;


        $ip_licenses = DB::table('ip_license_ticket')
                            ->select('ip_license_id')
                            ->where('ticket_id', $ticket_id)
                            ->get();


        //dd($ip_licenses);


        $validatedData = $request->validate([

            'status_id' => 'nullable'
                                

        ]);

        $ticket->update($validatedData);
        $ticket->save();


          if($request->status_id == 2){

               //Approved Ticket, then change License Status to Unavailable

               foreach ($ip_licenses as $ip_license) {

                   DB::table('ip_licenses')
                        ->where('id', $ip_license->ip_license_id)
                        ->update(['ip_license_status_id' => 2 ]);

                   

                  
               }

              return redirect(route('tickets.index', $ticket->id))
                ->with('message', "Ticket status is Approved. IP License status is now Unavailable.");

           }else{


               //Pending, Denied, or Completed Ticket, then change License Status to Available

               foreach ($ip_licenses as $ip_license) {

                   DB::table('ip_licenses')
                        ->where('id', $ip_license->ip_license_id)
                        ->update(['ip_license_status_id' => 1 ]);
                 }

            return redirect(route('tickets.index', $ticket->id))
                ->with('message', "Ticket status is updated successfully. IP License status is now Available.");


           }

       

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }
}
