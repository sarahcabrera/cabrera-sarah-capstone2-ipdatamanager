<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IpLicenseStatus extends Model
{
    protected $fillable = ["name"];


    //laravel relationship one-to-many
    public function ip_licenses()
    {
        return $this->hasMany('App\IpLicenses');
    }
}
