<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Ticket extends Model
{
    

    protected $fillable = [

    		"ticket_code",
    		"user_id",
    		"status_id",
    		"duration_in_months",
    		"date_needed",
    		"return_date"
    	
    ];

    //laravel relationship one-to-many (inverse)
	public function ticket_status()
	{
		return $this->belongsTo('App\TicketStatus');
	}


	public function user()
	{
		return $this->belongsTo('App\User');
	}


	// Many to Many relationship 
	public function ip_licenses(){

		return $this->belongsToMany('App\IpLicense')
				->withPivot('duration_in_months', 'date_needed', 'return_date', 'ip_license_id')
				->withTimestamps();
	}

	public function isTheOwner($user){

         return $this->user_id === $user->id;
  	}
}
