<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ip extends Model
{
    //
    use SoftDeletes;
	//make sure use SoftDeletes is first/topmost here
	//so it can be used for the whole model

    protected $fillable = ["name", "term", "protection_start", "description", "works_covered"];

    /* Get the Ip Assets that Belong to the IP Category */
    //laravel relationship one-to-many
    public function ip_assets()
    {
    	return $this->hasMany('App\IpAssets');
    }
}
