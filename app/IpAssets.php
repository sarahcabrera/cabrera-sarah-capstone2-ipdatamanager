<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

class IpAssets extends Model
{
    use SoftDeletes;
    //make sure use SoftDeletes is first/topmost here
    //so it can be used for the whole model
    
    protected $guarded = [];

    

    protected $fillable = [

    			"name",
                "creator",
    			"description",
                "status_id",

    			"image", "path", "auth_by", "size",


    			"ip_id",
                "type_id",
                "code",
                "license_limit",
                "inventory",
                "quantity_licensed",
    			"creation_date",
                "registered_date",
                "renewal_due_date", 
                "renewal_status_id" 


    ];


     /* Get the IP Category that owns the IP Asset */
    //laravel relationship one-to-many (inverse)
	public function ip()
	{
		return $this->belongsTo('App\Ip');
	}

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function renewal_status()
    {
        return $this->belongsTo('App\RenewalStatus');
    }


    //laravel relationship one-to-many
    public function ip_licenses()
    {
        return $this->hasMany('App\IpLicenses');
    }

	
    //protected $dateFormat = 'Y-m-d';



    /* @array $appends */
    public $appends = ['url', 'uploaded_time', 'size_in_kb'];
    
    public function getUrlAttribute()
    {
        return Storage::disk('s3')->url($this->path);
    }
    public function getUploadedTimeAttribute()
    {
        return $this->created_at->diffForHumans();
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'auth_by');
    }
    public function getSizeInKbAttribute()
    {
        return round($this->size / 1024, 2);
    }
    public static function boot()
    {
        parent::boot();
        static::creating(function ($image) {
            $image->auth_by = auth()->user()->id;
        });
    }


}