<?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::delete('/license_request', 'LicenseRequestController@clear')->name('license_request.clear');
Route::get('tickets.pending', 'TicketController@pending')->name('tickets.pending');
Route::get('tickets.approved', 'TicketController@approved')->name('tickets.approved');
Route::get('tickets.declined', 'TicketController@declined')->name('tickets.declined');
Route::get('tickets.completed', 'TicketController@completed')->name('tickets.completed');
Route::get('tickets.recent', 'TicketController@recent')->name('tickets.recent');

Route::get('tickets/user/{id}', 'TicketController@user')->name('tickets.user');


Route::resources([

	'ips' => 'IpController',
	'types' => 'TypeController',
	'ip_assets' => 'IpAssetsController',
	'ip_licenses' => 'IpLicenseController',
	'license_request' => 'LicenseRequestController',
	'tickets' => 'TicketController'
]);




