@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Edit IP Category
				</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-12 col-md-6 mx-auto">
				{{-- start form --}}
				<form action="{{ route('ips.update', $ip->id) }}" method="post">
					@method('PUT')
					@csrf

					<label for="name">IP Category Name:</label>
					<input 
						id="name" 
						type="text" 
						class="form-control 
								@error('name') is-invalid @enderror" 
						name="name" 
						value="{{ $ip->name }}" 
						required 
						autocomplete="name" 
						autofocus
					>

                    @error('name')
                        <span class="invalid-feedback d-block" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                    <label for="term">Protection Term:</label>
					<input 
						id="term" 
						type="text" 
						class="form-control 
								@error('term') is-invalid @enderror" 
						name="term" 
						value="{{ $ip->term }}" 
						required 
						autocomplete="term" 
						autofocus
					>
                    @error('term')
                        <span class="invalid-feedback d-block" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                    <label for="protection_start">Protection Start:</label>
					<input 
						id="protection_start" 
						type="text" 
						class="form-control 
								@error('protection_start') is-invalid @enderror" 
						name="protection_start" 
						value="{{ $ip->protection_start }}" 
						required 
						autocomplete="protection_start" 
						autofocus
					>
                    @error('protection_start')
                        <span class="invalid-feedback d-block" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror


                    <label for="description">Description:</label>
					<input 
						id="description" 
						type="text" 
						class="form-control 
								@error('description') is-invalid @enderror" 
						name="description" 
						value="{{ $ip->description }}" 
						required 
						autocomplete="description" 
						autofocus
					>
                    @error('description')
                        <span class="invalid-feedback d-block" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                    <label for="works_covered">Works Covered:</label>
					<input 
						id="works_covered" 
						type="text" 
						class="form-control 
								@error('works_covered') is-invalid @enderror" 
						name="works_covered" 
						value="{{ $ip->works_covered }}" 
						required 
						autocomplete="works_covered" 
						autofocus
					>
                    @error('works_covered')
                        <span class="invalid-feedback d-block" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

					<button class="btn btn-sm btn-warning mt-1">Edit</button>
				</form>
				{{-- start form --}}
			</div>
				}
		</div>
	</div>


@endsection


