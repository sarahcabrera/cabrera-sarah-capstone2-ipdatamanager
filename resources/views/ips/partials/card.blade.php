{{-- card start --}}
	<div class="card mb-3">

		<div class="card-body">
			<h4 class="card-title text-center">
				{{ $ip->name }}
			</h4>
			
			@if(!isset($index))
				<h5 class="btn text-justify my-2 w-100"
					style="border:solid thin lightseagreen; padding: 10px; background-color: #96ceb4">
					<b>Protection Term:</b> {{ $ip->term }}
				</h5>
				<h5 class="btn btn-warning text-justify my-2 w-100"
					style="border:solid thin lightseagreen; padding: 10px; background-color: #ffeead">
					<b>Protection Start:</b> {{ $ip->protection_start }}
				</h5>
				<p class="btn text-justify my-2 w-100" 
					style="border:solid thin lightseagreen; padding: 10px; background-color: #ffcc5c">
					<b>Description:</b> {{ $ip->description }}
				</p> 
				<p class="btn text-justify my-2 w-100" 
					style="border:solid thin lightseagreen; padding: 10px; background-color: #ff6f69">
					<b>Works Covered:</b> {{ $ip->works_covered }}
				</p> 
			@endif 

		</div>

		<div class="card-footer text-center">

			@if(!isset($view))
				{{-- view --}}
				<a href="{{ route('ips.show', $ip->id ) }}" class="btn btn-success btn-sm mx-2">
					View
				</a>
			@endif  

			@can('isAdmin')
			{{-- edit --}}
			<a href="{{ route('ips.edit', $ip->id ) }}" class="btn btn-warning btn-sm mx-2">
				Edit
			</a>

			{{-- delete --}}
			<form action="{{ route('ips.destroy', $ip->id ) }}" method="post" class="d-inline-block mx-2">
				@csrf
				@method('DELETE')
				<button class="btn btn-danger btn-sm">
					Delete
				</button>
			</form>
			@endcan
			
		</div>

	</div>
{{-- card end --}}