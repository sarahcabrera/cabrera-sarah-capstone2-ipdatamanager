@extends('layouts.app')

@section('content')

	<div class="container">
		{{-- header start --}}
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Intellectual Property Categories
				</h1>
			</div>
		</div>
		{{-- header end --}}

		{{-- alert-message --}}
		@includeWhen(Session::has('message'),'partials.alert') 

		{{-- ip list start --}}
		<div class="row">
			@foreach($ips as $ip)
				
				<div class="col-12 col-sm-6 col-md-6 col-lg-6 mx-auto">
				{{-- card start --}}
					@include('ips.partials.card', ['index' => false])
				{{-- card end --}}
				</div>
				
			@endforeach
			
		</div>
		{{-- ip list end--}}
	</div>


@endsection