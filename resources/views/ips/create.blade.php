@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Create New IP Category
				</h1>
			</div>
		</div>

		{{-- Displaying The Validation Errors --}}
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
	

		{{-- start form section --}}
		<div class="row">
			<div class="col-12 col-sm-6 mx-auto">
				<form action="{{ route('ips.store') }}" method="post">
					@csrf
					<label for="name">Category name:</label>
					<input type="text" name="name" id="name" class="form-control form-control-sm autofocus">
					<label for="term">Protection Term:</label>
					<input type="text" name="term" id="term" class="form-control form-control-sm autofocus">
					<label for="protection_start">Protection start:</label>
					<input type="text" name="protection_start" id="protection_start" class="form-control form-control-sm autofocus">
					<label for="description">Description:</label>
					<input type="text" name="description" id="description" class="form-control form-control-sm autofocus">
					<label for="works_covered">Works Covered:</label>
					<input type="text" name="works_covered" id="works_covered" class="form-control form-control-sm autofocus">
					<button class="btn btn-sm btn-primary mt-1">Create IP Category</button>	
				</form>
			</div>
		</div>
		{{-- end form section --}}

	</div>

@endsection