{{-- card start --}}
	<div class="card mb-3">

		<div class="card-body">
			<h4 class="card-title text-center">
				{{ $type->name }}
			</h4>
		</div>

		<div class="card-footer text-center">

			@if(!isset($view))
				{{-- view --}}
				<a href="{{ route('types.show', $type->id ) }}" class="btn btn-success btn-sm mx-2">
					View
				</a>
			@endif 

			@can('isAdmin')
			{{-- edit --}}
			<a href="{{ route('types.edit', $type->id ) }}" class="btn btn-warning btn-sm mx-2">
				Edit
			</a>

			{{-- delete --}}
			<form action="{{ route('types.destroy', $type->id ) }}" method="post" class="d-inline-block mx-2">
				@csrf
				@method('DELETE')
				<button class="btn btn-danger btn-sm">
					Delete
				</button>
			</form>
			@endcan
			
		</div>

	</div>
{{-- card end --}}