@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Edit Type
				</h1>
			</div>
		</div>


		<div class="row">
			<div class="col-12 col-md-6 mx-auto">
				{{-- start form --}}
				<form action="{{ route('types.update', $type->id) }}" method="post">
					@csrf
					@method('PUT')
					<label for="name">Type name:</label>

					<input 
						id="name" 
						type="text" 
						class="form-control 
								@error('name') is-invalid @enderror" 
						name="name" 
						value="{{ $type->name }}" 
						required 
						autocomplete="name" 
						autofocus
					>

                    @error('name')
                        <span class="invalid-feedback d-block" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror



					<button class="btn btn-sm btn-warning mt-1">Edit</button>
				</form>
				{{-- start form --}}
			</div>
				}
		</div>
	</div>


@endsection


