@extends('layouts.app')

@section('content')

	<div class="container">
		{{-- header start --}}
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Types of Assets
				</h1>
			</div>
		</div>
		{{-- header end --}}

		{{-- alert-message --}}
		@includeWhen(Session::has('message'),'partials.alert')

		{{-- type list start --}}
		<div class="row">
			@foreach($types as $type)
				{{-- card start --}}
				<div class="col-12 col-sm-6 col-md-4 col-lg-3">
					@include('types.partials.card')
				</div>
				{{-- card end --}}
			@endforeach
			
		</div>
		{{-- type list end--}}
	</div>


@endsection