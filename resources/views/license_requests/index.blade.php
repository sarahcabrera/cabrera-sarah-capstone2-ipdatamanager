@extends('layouts.app')
@section('content')

  
  {{-- alert-message --}}
  @includeWhen(Session::has('message'),'partials.alert')
    

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">
                    My IP Asset License Requests
                </h1>
            </div>
        </div>

        @if(!Session::has('license_request'))
            {{-- alert start --}}
            <div class="alert alert-info alert-dismissible fade show text-center" 
                 role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong>You have no license requests!</strong> 
            </div>
            {{-- alert end --}}
        @else

            <div class="row">
                <div class="col-12">
                    {{-- start request table --}}

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name of IP Asset </th>
                                <th>Asset Code</th>
                                <th>License Code</th> 
                                <th>Duration</th> 
                                <th>Date Needed</th> 
                                <th>Return Date</th> 
                                <th>Update Request</th>
                                <th>Status</th> 
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($ip_licenses as $ip_license)
                                {{-- start item row --}}
                                <tr>
                                    <td scope="row">{{ $ip_license->ip_asset->name }} </td>

                                    <td scope="row">{{ $ip_license->ip_asset->code }} </td>

                                   <td scope="row">{{ $ip_license->code  }} </td>

                                   <td scope="row">

                                    {{ $ip_license->duration_in_months  }} month(s) 


                                   </td>

                                   <td scope="row">{{ date('M d Y', strtotime($ip_license->date_needed) ) }} </td>

                                   <td scope="row">{{ date('M d Y', strtotime($ip_license->return_date) )   }} </td>

                                   <td>

                                    @include('license_requests.partials.update-request')

                                   </td>

                                    <td scope="row">{{ $ip_license->ip_license_status->name }} </td> 
                                    <td>

                                        <form action="{{ route('license_request.destroy', $ip_license->id )}}" method="post">
                                          @csrf
                                          @method('DELETE')
                                          <button class="btn-sm btn-danger">
                                            Remove
                                          </button>
                                        </form>
                                    </td>

                                     
                                </tr>
                                {{-- end item row --}}
                                <tr>

                                
                                  

                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot class="mt-3">
                            <tr>
                                <td colspan="7">
                                  
                                </td>
                                <td class="ml-5" colspan="5">
                                  @guest 
                                      <a href="{{ route('login') }}" class="btn btn-sm btn-success w-100 my-3">Login to checkout</a>
                                  @else

                                      <form action="{{ route('tickets.store') }}" 
                                            method="post"
                                       >
                                          @csrf
                                          @method('POST')
                                          <button type="submit" class="btn-sm btn-success w-100">
                                            Checkout
                                          </button>      
                                      </form>
                                      
                                  @endguest
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                    {{-- end request table --}}
                </div>
            </div>

            {{-- clear request --}}
            <div class="row">
                <div class="col-12">
                    <form action="{{ route('license_request.clear')}}" method="post">
                      @csrf
                      @method('DELETE')
                      <button class="btn-sm btn-outline-danger">
                        Clear All Requests
                      </button>
                    </form>
                </div>
            </div>
        @endif

    </div>
    
@endsection