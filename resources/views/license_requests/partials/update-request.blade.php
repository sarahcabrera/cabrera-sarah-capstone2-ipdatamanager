<form action="{{ route('license_request.update', $ip_license->id) }}" method="post" class="my-2">
	@csrf
	@method('PUT')

	<label for="date_needed" class="my-1">Duration in Months:</label>

    <input type="hidden" id="set_duration" name="set_duration" 
            value="{{$ip_license->duration_in_months }}">

            
    <input type="number" 
    		name="duration_in_months" 
    		id="duration_in_months" 
    		value="{{$ip_license->duration_in_months }}" 
    		class="form-control form-control-sm">
    
    <label for="date_needed" class="my-1">License Start Date:</label>
    <input type="date" name="date_needed" id="date_needed" min="<?= date('Y-m-d'); ?>" class="form-control form-control-sm">
    <label for="return_date" class="my-1">License End Date:</label>
    <input type="date" name="return_date" id="return_date" min="<?= date('Y-m-d'); ?>" class="form-control form-control-sm">


	<button class="btn btn-small btn-success w-100 mt-1">Request</button>
</form>