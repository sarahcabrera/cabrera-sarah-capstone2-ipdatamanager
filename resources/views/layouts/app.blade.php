<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'IPDataManager') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <script src="sweetalert2.all.min.js"></script>
    <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Libre+Barcode+128&display=swap" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css2?family=Buda:wght@300&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

     <style>

      body {

       font-family: 'Buda', cursive;
       font-weight: 800;
       color: #13070C;
       /*background: 
         linear-gradient(to left,rgba(240, 240, 230, 0.8), rgba(70, 70, 70, 0.5)),
          url('https://ipdatamanager.s3.amazonaws.com/images/pexels-junghua-liu-3063362.jpg');*/
      }

      /* Extra small devices (phones, 600px and down) */
          @media only screen and (max-width: 600px) {


            .alert {
              position: relative;
              left: -65px;
              font-family: 'Nunito', sans-serif;
              padding: 1rem ;
              margin-bottom: 1rem;
              border: 1px solid transparent;
              border-radius: 0.25rem;
              font-size: 1rem;
              width: 20rem;
              opacity: .9;

            }

              

             .firefly1 {

             color: #00B0D2;
             font-size: 10em;
             opacity: .9;
             position: relative;
             animation-name: s;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes s{
               0%   {opacity: .2; left: 0px; bottom: 500px; }
               25%  {opacity: .3; left: -30px; bottom: 600px;}
               50%  {opacity: .5; left: 50px; bottom: 850px;}
               75%  {opacity: .4; left: -20px; bottom: 900px;}
               100% {opacity: .2; left: -50px; bottom: 1200px;}
             }

             .firefly2 {

             color: #FEE7A4;
             font-size: 7em;
             opacity: .7;
             position: relative;
             animation-name: g;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes g{
               0%   {opacity: .2; left: 10px; bottom: 400px; }
               25%  {opacity: .3; left: 60px; bottom: 500px;}
               50%  {opacity: .5; left: -30px; bottom: 750px;}
               75%  {opacity: .4; left: 0px; bottom: 850px;}
               100% {opacity: .2; left: -40px; bottom: 950px;}
             }


             .firefly3 {

             color: #EFE6DD;
             font-size: .6em;
             opacity: .8;
             position: relative;
             animation-name: b;
             animation-duration: 13s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes b{
               0%   {opacity: .2; left: 40px; bottom: 300px; }
               25%  {opacity: .3; left: 30px; bottom: 400px;}
               50%  {opacity: .5; left: -20px; bottom: 650px;}
               75%  {opacity: .4; left: -30px; bottom: 700px;}
               100% {opacity: .2; left: -50px; bottom: 850px;}
             }


             .firefly4 {

             color: #EFE6DD;
             font-size: .7em;
             opacity: .8;
             position: relative;
             animation-name: p;
             animation-duration: 6s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes p{
               0%   {opacity: .3; left: 50px; bottom: 550px; }
               25%  {opacity: .5; left: 20px; bottom: 650px;}
               50%  {opacity: .6; left: 0px; bottom: 750px;}
               75%  {opacity: .4; left: -10px; bottom: 800px;}
               100% {opacity: .3; left: -30px; bottom: 850px;}
             }


             .firefly5 {

             color: #EFE6DD;
             font-size: .7em;
             opacity: .7;
             position: relative;
             animation-name: q;
             animation-duration: 8s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes q{
               0%   {opacity: .3; left: 0px; bottom: 300px; }
               25%  {opacity: .5; left: 10px; bottom: 450px;}
               50%  {opacity: .6; left: 30px; bottom: 550px;}
               75%  {opacity: .4; left: 50px; bottom: 700px;}
               100% {opacity: .3; left: 10px; bottom: 850px;}
             }


             .firefly6 {

             color: #EFE6DD;
             font-size: .6em;
             opacity: .7;
             position: relative;
             animation-name: a;
             animation-duration: 14s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes a{
               0%   {opacity: .6; left: 0px; bottom: 400px; }
               25%  {opacity: .8; left: -50px; bottom: 550px;}
               50%  {opacity: .5; left: -70px; bottom: 650px;}
               75%  {opacity: .8; left: -100px; bottom: 800px;}
               100% {opacity: .4; left: -120px; bottom: 950px;}
             }


             .firefly7 {

             color: #EFE6DD;
             font-size: .6em;
             opacity: .7;
             position: relative;
             animation-name: levitate1;
             animation-duration: 14s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes levitate1{
               0%   {opacity: .6; left: -30px; bottom: 400px; }
               25%  {opacity: .8; left: -100px; bottom: 550px;}
               50%  {opacity: .5; left: -200px; bottom: 650px;}
               75%  {opacity: .8; left: -300px; bottom: 900px;}
               100% {opacity: .4; left: -20px; bottom: 950px;}
             }


              .firefly8 {

             color: #EFE6DD;
             font-size: .7em;
             opacity: .7;
             position: relative;
             animation-name: levitate2;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes levitate2{
               0%   {opacity: .6; left: -70px; bottom: 500px; }
               25%  {opacity: .8; left: -60px; bottom: 600px;}
               50%  {opacity: .5; left: -40px; bottom: 700px;}
               75%  {opacity: .8; left: 30px; bottom: 800px;}
               100% {opacity: .4; left: -10px; bottom: 950px;}
             }




          }

          /* Medium devices (landscape tablets, 768px and up) */
          @media only screen and (min-width: 767px)  {

              .alert {
                position: relative;
                font-family: 'Nunito', sans-serif;
                top: 2%;
                left: -28%;
                padding: 1rem ;
                margin-bottom: 1rem;
                border: 1px solid transparent;
                border-radius: 0.25rem;
                font-size: 1rem;
                width: 40rem;
                opacity: .85;

              }
             
             .firefly1 {

             color: #00B0D2;
             font-size: 12em;
             opacity: .9;
             position: relative;
             animation-name: s;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes s{
               0%   {opacity: .2; left: 100px; bottom: 300px; }
               25%  {opacity: .3; left: 80px; bottom: 500px;}
               50%  {opacity: .5; left: 60px; bottom: 750px;}
               75%  {opacity: .4; left: 40px; bottom: 800px;}
               100% {opacity: .2; left: -10px; bottom: 900px;}
             }

             .firefly2 {

             color: #FEE7A4;
             font-size: 7em;
             opacity: .7;
             position: relative;
             animation-name: g;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes g{
               0%   {opacity: .2; left: 60px; bottom: 300px; }
               25%  {opacity: .3; left: 30px; bottom: 400px;}
               50%  {opacity: .5; left: 10px; bottom: 650px;}
               75%  {opacity: .4; left: 0px; bottom: 700px;}
               100% {opacity: .2; left: -20px; bottom: 850px;}
             }


             .firefly3 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .8;
             position: relative;
             animation-name: b;
             animation-duration: 13s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes b{
               0%   {opacity: .2; left: 140px; bottom: 300px; }
               25%  {opacity: .3; left: 120px; bottom: 400px;}
               50%  {opacity: .5; left: 90px; bottom: 650px;}
               75%  {opacity: .4; left: 80px; bottom: 700px;}
               100% {opacity: .2; left: 50px; bottom: 850px;}
             }


             .firefly4 {

             color: #EFE6DD;
             font-size: 2em;
             opacity: .8;
             position: relative;
             animation-name: p;
             animation-duration: 6s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes p{
               0%   {opacity: .3; left: 120px; bottom: 550px; }
               25%  {opacity: .5; left: 110px; bottom: 650px;}
               50%  {opacity: .6; left: 100px; bottom: 750px;}
               75%  {opacity: .4; left: 70px; bottom: 800px;}
               100% {opacity: .3; left: 10px; bottom: 850px;}
             }


             .firefly5 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: q;
             animation-duration: 8s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes q{
               0%   {opacity: .3; left: 0px; bottom: 300px; }
               25%  {opacity: .5; left: 10px; bottom: 450px;}
               50%  {opacity: .6; left: 30px; bottom: 550px;}
               75%  {opacity: .4; left: 70px; bottom: 700px;}
               100% {opacity: .3; left: 90px; bottom: 850px;}
             }


             .firefly6 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: a;
             animation-duration: 14s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes a{
               0%   {opacity: .6; left: 0px; bottom: 400px; }
               25%  {opacity: .8; left: -50px; bottom: 550px;}
               50%  {opacity: .5; left: -70px; bottom: 650px;}
               75%  {opacity: .8; left: -100px; bottom: 800px;}
               100% {opacity: .4; left: -120px; bottom: 950px;}
             }


             .firefly7 {

             color: #EFE6DD;
             font-size: 2em;
             opacity: .7;
             position: relative;
             animation-name: levitate1;
             animation-duration: 14s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes levitate1{
               0%   {opacity: .6; left: -30px; bottom: 400px; }
               25%  {opacity: .8; left: -100px; bottom: 550px;}
               50%  {opacity: .5; left: -200px; bottom: 650px;}
               75%  {opacity: .8; left: -300px; bottom: 900px;}
               100% {opacity: .4; left: -450px; bottom: 950px;}
             }


              .firefly8 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: levitate2;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes levitate2{
               0%   {opacity: .6; left: -70px; bottom: 500px; }
               25%  {opacity: .8; left: -60px; bottom: 600px;}
               50%  {opacity: .5; left: -40px; bottom: 700px;}
               75%  {opacity: .8; left: 50px; bottom: 800px;}
               100% {opacity: .4; left: 100px; bottom: 950px;}
             }




          }

         

          /* Large devices (laptops/desktops, 992px and up) */
          @media only screen and (min-width: 1024px) {


             .alert {
                position: relative;
                font-family: 'Nunito', sans-serif;
                top: 2%;
                left: 0%;
                padding: 1rem ;
                margin-bottom: 1rem;
                border: 1px solid transparent;
                border-radius: 0.25rem;
                font-size: 1rem;
                width: 47rem;
                opacity: .85;

              }


            .firefly1 {

             color: #00B0D2;
             font-size: 13em;
             opacity: .4;
             position: relative;
             animation-name: s;
             animation-duration: 4s;
             animation-iteration-count: 1;
             animation-timing-function: linear;

             }

             @keyframes s{
               0%   {opacity: .2; left: 300px; bottom: 500px; }
               25%  {opacity: .4; left: 250px; bottom: 700px;}
               50%  {opacity: .7; left: 200px; bottom: 850px;}
               75%  {opacity: .3; left: 100px; bottom: 900px;}
               100% {opacity: .1; left: 10px; bottom: 1200px;}
             }

             .firefly2 {

             color: #FEE7A4;
             font-size: 8em;
             opacity: .4;
             position: relative;
             animation-name: g;
             animation-duration: 4s;
             animation-iteration-count: 1;
             animation-timing-function: linear;

             }

             @keyframes g{
               0%   {opacity: .4; left: 200px; bottom: 300px; }
               25%  {opacity: .6; left: -10px; bottom: 400px;}
               50%  {opacity: .7; left: -50px; bottom: 600px;}
               75%  {opacity: .5; left: -100px; bottom: 850px;}
               100% {opacity: .3; left: -300px; bottom: 1000px;}
             }


             .firefly3 {

             color: #EFE6DD;
             font-size: 4em;
             opacity: .8;
             position: relative;
             animation-name: b;
             animation-duration: 13s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes b{
               0%   {opacity: .4; left: 100px; bottom: 200px; }
               25%  {opacity: .6; left: 250px; bottom: 350px;}
               50%  {opacity: .7; left: 300px; bottom: 500px;}
               75%  {opacity: .5; left: 200px; bottom: 750px;}
               100% {opacity: .3; left: 100px; bottom: 800px;}
             }


             .firefly4 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .8;
             position: relative;
             animation-name: p;
             animation-duration: 20s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes p{
               0%   {opacity: .3; left: 10px; bottom: 450px; }
               25%  {opacity: .5; left: 100px; bottom: 550px;}
               50%  {opacity: .6; left: 200px; bottom: 650px;}
               75%  {opacity: .4; left: 300px; bottom: 700px;}
               100% {opacity: .3; left: 230px; bottom: 900px;}
             }


             .firefly5 {

             color: #EFE6DD;
             font-size: 2em;
             opacity: .7;
             position: relative;
             animation-name: q;
             animation-duration: 8s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes q{
               0%   {opacity: .3; left: 40px; bottom: 10px; }
               25%  {opacity: .5; left: 120px; bottom: 150px;}
               50%  {opacity: .6; left: 200px; bottom: 250px;}
               75%  {opacity: .4; left: 220px; bottom: 300px;}
               100% {opacity: .3; left: 250px; bottom: 650px;}
             }


             .firefly6 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: a;
             animation-duration: 14s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes a{
               0%   {opacity: .6; left: 10px; bottom: 10px; }
               25%  {opacity: .8; left: -100px; bottom: 50px;}
               50%  {opacity: .5; left: -300px; bottom: 150px;}
               75%  {opacity: .8; left: -400px; bottom: 400px;}
               100% {opacity: .4; left: -500px; bottom: 750px;}
             }


             .firefly7 {

             color: #EFE6DD;
             font-size: 2em;
             opacity: .8;
             position: relative;
             animation-name: levitate1;
             animation-duration: 8s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes levitate1{
               0%   {opacity: .6; left: -10px; bottom: 300px; }
               25%  {opacity: .8; left: -50px; bottom: 200px;}
               50%  {opacity: .5; left: -100px; bottom: 100px;}
               75%  {opacity: .8; left: -30px; bottom: 400px;}
               100% {opacity: .4; left: -10px; bottom: 650px;}
             }


              .firefly8 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: levitate2;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes levitate2{
               0%   {opacity: .6; left: -100px; bottom: 200px; }
               25%  {opacity: .8; left: -200px; bottom: 500px;}
               50%  {opacity: .5; left: -300px; bottom: 700px;}
               75%  {opacity: .8; left: -400px; bottom: 800px;}
               100% {opacity: .4; left: -50px; bottom: 950px;}
             }


          }

            
     </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow-sm col-12"
              style="font-size: 1.1em; 
                     font-family: 'Nunito', sans-serif;
                     text-shadow: 2px 2px 2px maroon;
                     background: 
                       linear-gradient(to top, rgba(245, 240, 230, 0.8), rgba(60, 30, 20, 0.9)),
                                url('https://ipdatamanager.s3.us-east-1.amazonaws.com/images/pexels-nextvoyage-3053859%20%281%29.jpg');">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'IPDataManager') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse navbar-dark" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @can('isAdmin')
                        {{-- start admin dropdown --}}
                        <li class="nav-item dropdown">
                            <a id="adminDropdown" href="#" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown">
                                Admin
                            </a>
                            <div class="dropdown-menu"
                                style="text-shadow: 2px 2px 2px peachpuff; 
                                      background: 
                                       linear-gradient(to right,rgba(245, 250, 220, 0.99), rgba(200, 90, 100, 0.8))";>

                                <h5 class="ml-4"> <i class="fa fa-plus" aria-hidden="true"></i>ADD: 
                                </h5>

                               {{-- add IP Category  --}}
                              <a href="{{ route('ips.create') }}" 
                                 class="dropdown-item
                                        {{ Route::CurrentRouteNamed('ips.create') ? "active" : "" }}
                                "
                                  style="text-shadow: 2px 2px 2px peachpuff;" 
                                >
                                <i class="fa fa-registered" aria-hidden="true"></i>
                               Category
                              </a>

                              {{-- add IP Type of Asset  --}}
                              <a href="{{ route('types.create') }}" 
                                 class="dropdown-item
                                        {{ Route::CurrentRouteNamed('types.create') ? "active" : "" }}
                                "
                                    style="text-shadow: 2px 2px 2px peachpuff;" 

                                >
                                <i class="fa fa-tags" aria-hidden="true"></i>
                                Type of Asset
                              </a>

                              {{-- add IP Asset --}}
                              <a href="{{ route('ip_assets.create') }}" 
                                 class="dropdown-item
                                        {{ Route::CurrentRouteNamed('ip_assets.create') ? "active" : "" }}
                              "
                                  style="text-shadow: 2px 2px 2px peachpuff;" 
                              >
                                <i class="fa fa-diamond" aria-hidden="true"></i>
                                Asset
                              </a> 

                              {{-- add IP License --}}
                              <a href="{{ route('ip_licenses.create') }}" 
                                 class="dropdown-item
                                        {{ Route::CurrentRouteNamed('ip_licenses.create') ? "active" : "" }}
                              "
                                  style="text-shadow: 2px 2px 2px peachpuff;" 
                              >
                                <i class="fa fa-id-card-o" aria-hidden="true"></i>
                                License
                              </a> 
                            </div>
                        </li>
                        {{-- end admin dropdown --}}
                        @endcan
                      @if (Route::has('login'))
                        @auth
                        {{-- category index --}}
                        <li class="nav-item">
                            <a href="{{ route('ips.index') }}" 
                                class="nav-link
                                      {{ Route::CurrentRouteNamed('ips.index') ? "active" : "" }}
                            ">
                                <i class="fa fa-registered" aria-hidden="true"></i> Categories
                            </a>
                        </li>

                        {{-- type index --}}
                        <li class="nav-item">
                            <a href="{{ route('types.index') }}" 
                                class="nav-link
                                      {{ Route::CurrentRouteNamed('types.index') ? "active" : "" }}
                            ">
                               <i class="fa fa-tags" aria-hidden="true"></i> Asset Types
                            </a>
                        </li>
                        
                       {{--  ip_assets index --}}
                        <li class="nav-item">
                            <a href="{{ route('ip_assets.index') }}" 
                               class="nav-link
                                      {{ Route::CurrentRouteNamed('ip_assets.index') ? "active" : "" }}
                            ">
                               <i class="fa fa-diamond" aria-hidden="true"></i> Assets
                            </a>
                        </li> 
                        
                        
                        {{--  ip_licenses index --}}
                        <li class="nav-item">
                            <a href="{{ route('ip_licenses.index') }}" 
                               class="nav-link
                                      {{ Route::CurrentRouteNamed('ip_licenses.index') ? "active" : "" }}
                            ">
                               <i class="fa fa-id-card-o" aria-hidden="true"></i> Licenses
                            </a>
                        </li> 
                        


                        @endauth
                      @endif

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">

                      @if (Route::has('login'))
                        @auth
                      {{-- Requests --}}
                            @cannot('isAdmin')
                            <li class="nav-item">
                                <a class="nav-link
                                            {{ Route::CurrentRouteNamed('license_request.index') ? "active" : "" }}
                                " href="{{ route('license_request.index') }}">

                                Requests 
                                <span class="badge badge-primary">
                                    {{ Session::has('license_request') ? count(Session::get('license_request')) : 0 }}
                                </span>

                              </a>
                            </li>
                            @endcannot
                        {{-- Tickets --}}

                            <li class="nav-item">
                                <a class="nav-link
                                            {{ Route::CurrentRouteNamed('tickets.index') ? "active" : "" }}
                                " href="{{ route('tickets.index') }}">

                                <i class="fa fa-ticket" aria-hidden="true"></i>
                                Tickets 
                      
                              </a>
                            </li>
                        @endauth
                      @endif







                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link
                                            {{ Route::CurrentRouteNamed('login') ? "active" : "" }}
                                " href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link
                                              {{ Route::CurrentRouteNamed('register') ? "active" : "" }}
                                    " href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown"
                                  style="text-shadow: 2px 2px 2px peachpuff;
                                        background: 
                                         linear-gradient(to right,rgba(245, 250, 220, 0.95), rgba(200, 90, 100, 0.85));">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">

                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>


        <div class="mx-auto mt-5">
          <div class="row">
           <span class="badge firefly1"><i class="fa fa-cog fa-spin"></i></span>
           <span class="badge firefly2 mt-5 pt-5"><i class="fa fa-cog fa-spin"></i></span>
           <span class="badge firefly3"><i class="fa fa-cog fa-spin"></i></span>
           <span class="badge firefly4"><i class="fa fa-cog fa-spin"></i></span>
           <span class="badge firefly5"><i class="fa fa-cog fa-spin"></i></span>
           <span class="badge firefly6"><i class="fa fa-cog fa-spin"></i></span>
           <span class="badge firefly7"><i class="fa fa-cog fa-spin"></i></span>
           <span class="badge firefly8"><i class="fa fa-cog fa-spin"></i></span>
          </div>

          <div class="row mx-auto mt-5 mb-2">
            <div class="col-11 mx-auto mt-5">
              <span class="badge badge-success ml-5" style="opacity: .6;"><b>&#169;</b> S.Cabrera 2020</span>
              <span class="badge badge-warning" style="opacity: .6;">Education Purposes</span>
            </div>
           
          </div>

        </div>

        
    </div>

   


    
</body>
</html>
