<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>IPDataManager</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Josefin+Slab&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Buda:wght@300&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        

         <!-- Styles -->
        <style>

          /* Extra small devices (phones, 600px and down) */
          @media only screen and (max-width: 600px) {

              html, body {
               background-color: #fff;
               color: #636b6f;
               font-family: 'Buda', cursive;
               font-weight: 200;
               height: 100vh;
               margin: 0;
               background: 
                    linear-gradient(to right,rgba(240, 200, 180, 0.9), rgba(70, 50, 20, 0.7)),
                    url('https://ipdatamanager.s3.amazonaws.com/images/pexels-nextvoyage-3053859+(1).jpg');
                
                background-size: 260%;
                background-position: -220px;
                
              }


             .full-height {
             height: 100vh;
             }


             .flex-center {
               align-items: center;
               display: flex;
               justify-content: center;
             }

             .position-ref {
               position: relative;
             }

             .top-right {
               position: absolute;
               right: 10px;
               top: 18px;
             }

             .content {
               text-align: center;
             }

             .tagline {

               font-size: .9rem;
               color: #FAF6F6 ;
               text-shadow: 2px 2px 2px  #0150A2 ;
               position: relative;

             }

             .title {
             font-size: 2.4rem;
             color: #FAF6F6 ;
             text-shadow: 5px 5px 5px #0150A2 ;
             position: relative;
             animation-name: title;
             animation-duration: 5s;
             animation-iteration-count: infinite;
             animation-direction: alternate;
             animation-timing-function: ease;

             }

             @keyframes title{
               0%   {text-shadow: 6px 6px 6px  #930031; }
               25%  {text-shadow: 6px 6px 6px  #0150A2; }
               50%  {text-shadow: 6px 6px 6px #EB365C; }
               75%  {text-shadow: 6px 6px 6px  #00B0D2; }
               100% {text-shadow: 6px 6px 6px  #930031; }
             }



             .links > a {
             color: #FAF6F6;
             padding: 0 25px;
             font-size: .8rem;
             font-weight: 600;
             letter-spacing: .1rem;
             text-decoration: none;
             text-transform: uppercase;
             text-shadow: 3px 3px 3px #0150A2;
             }

             .m-b-md {
             margin-bottom: 30px;
             }

             .firefly1 {

             color: #00B0D2;
             font-size: 12em;
             opacity: .9;
             position: relative;
             animation-name: s;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes s{
               0%   {opacity: .2; left: 100px; bottom: 300px; }
               25%  {opacity: .3; left: 80px; bottom: 500px;}
               50%  {opacity: .5; left: 60px; bottom: 750px;}
               75%  {opacity: .4; left: 40px; bottom: 800px;}
               100% {opacity: .2; left: -10px; bottom: 900px;}
             }

             .firefly2 {

             color: #FEE7A4;
             font-size: 7em;
             opacity: .7;
             position: relative;
             animation-name: g;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes g{
               0%   {opacity: .2; left: 60px; bottom: 300px; }
               25%  {opacity: .3; left: 30px; bottom: 400px;}
               50%  {opacity: .5; left: 10px; bottom: 650px;}
               75%  {opacity: .4; left: 0px; bottom: 700px;}
               100% {opacity: .2; left: -20px; bottom: 850px;}
             }


             .firefly3 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .8;
             position: relative;
             animation-name: b;
             animation-duration: 13s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes b{
               0%   {opacity: .2; left: 140px; bottom: 300px; }
               25%  {opacity: .3; left: 120px; bottom: 400px;}
               50%  {opacity: .5; left: 90px; bottom: 650px;}
               75%  {opacity: .4; left: 80px; bottom: 700px;}
               100% {opacity: .2; left: 50px; bottom: 850px;}
             }


             .firefly4 {

             color: #EFE6DD;
             font-size: 2em;
             opacity: .8;
             position: relative;
             animation-name: p;
             animation-duration: 6s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes p{
               0%   {opacity: .3; left: 120px; bottom: 550px; }
               25%  {opacity: .5; left: 110px; bottom: 650px;}
               50%  {opacity: .6; left: 100px; bottom: 750px;}
               75%  {opacity: .4; left: 70px; bottom: 800px;}
               100% {opacity: .3; left: 10px; bottom: 850px;}
             }


             .firefly5 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: q;
             animation-duration: 8s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes q{
               0%   {opacity: .3; left: 0px; bottom: 300px; }
               25%  {opacity: .5; left: 10px; bottom: 450px;}
               50%  {opacity: .6; left: 30px; bottom: 550px;}
               75%  {opacity: .4; left: 70px; bottom: 700px;}
               100% {opacity: .3; left: 90px; bottom: 850px;}
             }


             .firefly6 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: a;
             animation-duration: 14s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes a{
               0%   {opacity: .6; left: 0px; bottom: 400px; }
               25%  {opacity: .8; left: -50px; bottom: 550px;}
               50%  {opacity: .5; left: -70px; bottom: 650px;}
               75%  {opacity: .8; left: -100px; bottom: 800px;}
               100% {opacity: .4; left: -120px; bottom: 950px;}
             }


             .firefly7 {

             color: #EFE6DD;
             font-size: 2em;
             opacity: .7;
             position: relative;
             animation-name: levitate1;
             animation-duration: 14s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes levitate1{
               0%   {opacity: .6; left: -30px; bottom: 400px; }
               25%  {opacity: .8; left: -100px; bottom: 550px;}
               50%  {opacity: .5; left: -200px; bottom: 650px;}
               75%  {opacity: .8; left: -300px; bottom: 900px;}
               100% {opacity: .4; left: -450px; bottom: 950px;}
             }


              .firefly8 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: levitate2;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes levitate2{
               0%   {opacity: .6; left: -70px; bottom: 500px; }
               25%  {opacity: .8; left: -60px; bottom: 600px;}
               50%  {opacity: .5; left: -40px; bottom: 700px;}
               75%  {opacity: .8; left: 50px; bottom: 800px;}
               100% {opacity: .4; left: 100px; bottom: 950px;}
             }




          }

          /* Medium devices (landscape tablets, 768px and up) */
          @media only screen and (min-width: 767px)  {

             html, body {
               background-color: #fff;
               color: #636b6f;
               font-family: 'Buda', cursive;
               font-weight: 200;
               height: 100vh;
               margin: 0;
               background: 
                    linear-gradient(to right,rgba(240, 200, 180, 0.9), rgba(70, 50, 20, 0.7)),
                    url('https://ipdatamanager.s3.amazonaws.com/images/pexels-nextvoyage-3053859+(1).jpg');
                
                background-size: 140%;
                
              }


             .full-height {
             height: 65vh;
             }


             .flex-center {
               align-items: center;
               display: flex;
               justify-content: center;
             }

             .position-ref {
               position: relative;
             }

             .top-right {
               position: absolute;
               right: 100px;
               top: 18px;
             }

             .content {
               text-align: center;
               position: relative;
               right: 50px;
             }

             .tagline {

               font-size: .98rem;
               color: #FAF6F6 ;
               text-shadow: 2px 2px 2px  #0150A2 ;
               position: relative;

             }

             .title {
             font-size: 2.6rem;
             color: #FAF6F6 ;
             text-shadow: 5px 5px 5px #0150A2 ;
             position: relative;
             animation-name: title;
             animation-duration: 5s;
             animation-iteration-count: infinite;
             animation-direction: alternate;
             animation-timing-function: ease;

             }

             @keyframes title{
               0%   {text-shadow: 6px 6px 6px  #930031; }
               25%  {text-shadow: 6px 6px 6px  #0150A2; }
               50%  {text-shadow: 6px 6px 6px #EB365C; }
               75%  {text-shadow: 6px 6px 6px  #00B0D2; }
               100% {text-shadow: 6px 6px 6px  #930031; }
             }



             .links > a {
             color: #FAF6F6;
             padding: 0 25px;
             font-size: .8rem;
             font-weight: 600;
             letter-spacing: .1rem;
             text-decoration: none;
             text-transform: uppercase;
             text-shadow: 3px 3px 3px #0150A2;
             }

             .m-b-md {
             margin-bottom: 30px;
             }

             .firefly1 {

             color: #00B0D2;
             font-size: 12em;
             opacity: .9;
             position: relative;
             animation-name: s;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes s{
               0%   {opacity: .2; left: 100px; bottom: 300px; }
               25%  {opacity: .3; left: 80px; bottom: 500px;}
               50%  {opacity: .5; left: 60px; bottom: 750px;}
               75%  {opacity: .4; left: 40px; bottom: 800px;}
               100% {opacity: .2; left: -10px; bottom: 900px;}
             }

             .firefly2 {

             color: #FEE7A4;
             font-size: 7em;
             opacity: .7;
             position: relative;
             animation-name: g;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes g{
               0%   {opacity: .2; left: 60px; bottom: 300px; }
               25%  {opacity: .3; left: 30px; bottom: 400px;}
               50%  {opacity: .5; left: 10px; bottom: 650px;}
               75%  {opacity: .4; left: 0px; bottom: 700px;}
               100% {opacity: .2; left: -20px; bottom: 850px;}
             }


             .firefly3 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .8;
             position: relative;
             animation-name: b;
             animation-duration: 13s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes b{
               0%   {opacity: .2; left: 140px; bottom: 300px; }
               25%  {opacity: .3; left: 120px; bottom: 400px;}
               50%  {opacity: .5; left: 90px; bottom: 650px;}
               75%  {opacity: .4; left: 80px; bottom: 700px;}
               100% {opacity: .2; left: 50px; bottom: 850px;}
             }


             .firefly4 {

             color: #EFE6DD;
             font-size: 2em;
             opacity: .8;
             position: relative;
             animation-name: p;
             animation-duration: 6s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes p{
               0%   {opacity: .3; left: 120px; bottom: 550px; }
               25%  {opacity: .5; left: 110px; bottom: 650px;}
               50%  {opacity: .6; left: 100px; bottom: 750px;}
               75%  {opacity: .4; left: 70px; bottom: 800px;}
               100% {opacity: .3; left: 10px; bottom: 850px;}
             }


             .firefly5 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: q;
             animation-duration: 8s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes q{
               0%   {opacity: .3; left: 0px; bottom: 300px; }
               25%  {opacity: .5; left: 10px; bottom: 450px;}
               50%  {opacity: .6; left: 30px; bottom: 550px;}
               75%  {opacity: .4; left: 70px; bottom: 700px;}
               100% {opacity: .3; left: 90px; bottom: 850px;}
             }


             .firefly6 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: a;
             animation-duration: 14s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes a{
               0%   {opacity: .6; left: 0px; bottom: 400px; }
               25%  {opacity: .8; left: -50px; bottom: 550px;}
               50%  {opacity: .5; left: -70px; bottom: 650px;}
               75%  {opacity: .8; left: -100px; bottom: 800px;}
               100% {opacity: .4; left: -120px; bottom: 950px;}
             }


             .firefly7 {

             color: #EFE6DD;
             font-size: 2em;
             opacity: .7;
             position: relative;
             animation-name: levitate1;
             animation-duration: 14s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes levitate1{
               0%   {opacity: .6; left: -30px; bottom: 400px; }
               25%  {opacity: .8; left: -100px; bottom: 550px;}
               50%  {opacity: .5; left: -200px; bottom: 650px;}
               75%  {opacity: .8; left: -300px; bottom: 900px;}
               100% {opacity: .4; left: -450px; bottom: 950px;}
             }


              .firefly8 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: levitate2;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes levitate2{
               0%   {opacity: .6; left: -70px; bottom: 500px; }
               25%  {opacity: .8; left: -60px; bottom: 600px;}
               50%  {opacity: .5; left: -40px; bottom: 700px;}
               75%  {opacity: .8; left: 50px; bottom: 800px;}
               100% {opacity: .4; left: 100px; bottom: 950px;}
             }




          }

         

          /* Large devices (laptops/desktops, 992px and up) */
          @media only screen and (min-width: 1024px) {

              html, body {
               background-color: #fff;
               color: #636b6f;
               font-family: 'Buda', cursive;
               font-weight: 200;
               height: 100vh;
               margin: 0;
               background: 
                    linear-gradient(to right,rgba(240, 200, 180, 0.9), rgba(70, 50, 20, 0.7)),
                    url('https://ipdatamanager.s3.amazonaws.com/images/pexels-nextvoyage-3053859+(1).jpg');
                
                background-size: 150%;
                
              }


             .full-height {
             height: 80vh;
             }


             .flex-center {
               align-items: center;
               display: flex;
               justify-content: center;
             }

             .position-ref {
               position: relative;
             }

             .top-right {
               position: absolute;
               right: 150px;
               top: 18px;
             }

             .content {
               text-align: center;
               position: relative;
               right: 50px;
             }

             .tagline {

               font-size: 1.2rem;
               color: #FAF6F6 ;
               text-shadow: 2px 2px 2px  #0150A2 ;
               position: relative;

             }

             .title {
             font-size: 3rem;
             color: #FAF6F6 ;
             text-shadow: 5px 5px 5px #0150A2 ;
             position: relative;
             animation-name: title;
             animation-duration: 5s;
             animation-iteration-count: infinite;
             animation-direction: alternate;
             animation-timing-function: ease;

             }

             @keyframes title{
               0%   {text-shadow: 6px 6px 6px  #930031; }
               25%  {text-shadow: 6px 6px 6px  #0150A2; }
               50%  {text-shadow: 6px 6px 6px #EB365C; }
               75%  {text-shadow: 6px 6px 6px  #00B0D2; }
               100% {text-shadow: 6px 6px 6px  #930031; }
             }



             .links > a {
             color: #FAF6F6;
             padding: 0 25px;
             font-size: 1rem;
             font-weight: 600;
             letter-spacing: .1rem;
             text-decoration: none;
             text-transform: uppercase;
             text-shadow: 3px 3px 3px #0150A2;
             }

             .m-b-md {
             margin-bottom: 30px;
             }

             .firefly1 {

             color: #00B0D2;
             font-size: 12em;
             opacity: .9;
             position: relative;
             animation-name: s;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes s{
               0%   {opacity: .2; left: 100px; bottom: 300px; }
               25%  {opacity: .3; left: 80px; bottom: 500px;}
               50%  {opacity: .5; left: 60px; bottom: 750px;}
               75%  {opacity: .4; left: 40px; bottom: 800px;}
               100% {opacity: .2; left: -10px; bottom: 900px;}
             }

             .firefly2 {

             color: #FEE7A4;
             font-size: 7em;
             opacity: .7;
             position: relative;
             animation-name: g;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes g{
               0%   {opacity: .2; left: 60px; bottom: 300px; }
               25%  {opacity: .3; left: 30px; bottom: 400px;}
               50%  {opacity: .5; left: 10px; bottom: 650px;}
               75%  {opacity: .4; left: 0px; bottom: 700px;}
               100% {opacity: .2; left: -20px; bottom: 850px;}
             }


             .firefly3 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .8;
             position: relative;
             animation-name: b;
             animation-duration: 13s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes b{
               0%   {opacity: .2; left: 140px; bottom: 300px; }
               25%  {opacity: .3; left: 120px; bottom: 400px;}
               50%  {opacity: .5; left: 90px; bottom: 650px;}
               75%  {opacity: .4; left: 80px; bottom: 700px;}
               100% {opacity: .2; left: 50px; bottom: 850px;}
             }


             .firefly4 {

             color: #EFE6DD;
             font-size: 2em;
             opacity: .8;
             position: relative;
             animation-name: p;
             animation-duration: 6s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes p{
               0%   {opacity: .3; left: 120px; bottom: 550px; }
               25%  {opacity: .5; left: 110px; bottom: 650px;}
               50%  {opacity: .6; left: 100px; bottom: 750px;}
               75%  {opacity: .4; left: 70px; bottom: 800px;}
               100% {opacity: .3; left: 10px; bottom: 850px;}
             }


             .firefly5 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: q;
             animation-duration: 8s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes q{
               0%   {opacity: .3; left: 0px; bottom: 300px; }
               25%  {opacity: .5; left: 10px; bottom: 450px;}
               50%  {opacity: .6; left: 30px; bottom: 550px;}
               75%  {opacity: .4; left: 70px; bottom: 700px;}
               100% {opacity: .3; left: 90px; bottom: 850px;}
             }


             .firefly6 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: a;
             animation-duration: 14s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes a{
               0%   {opacity: .6; left: 0px; bottom: 400px; }
               25%  {opacity: .8; left: -50px; bottom: 550px;}
               50%  {opacity: .5; left: -70px; bottom: 650px;}
               75%  {opacity: .8; left: -100px; bottom: 800px;}
               100% {opacity: .4; left: -120px; bottom: 950px;}
             }


             .firefly7 {

             color: #EFE6DD;
             font-size: 2em;
             opacity: .7;
             position: relative;
             animation-name: levitate1;
             animation-duration: 14s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes levitate1{
               0%   {opacity: .6; left: -30px; bottom: 400px; }
               25%  {opacity: .8; left: -100px; bottom: 550px;}
               50%  {opacity: .5; left: -200px; bottom: 650px;}
               75%  {opacity: .8; left: -300px; bottom: 900px;}
               100% {opacity: .4; left: -450px; bottom: 950px;}
             }


              .firefly8 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: levitate2;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes levitate2{
               0%   {opacity: .6; left: -70px; bottom: 500px; }
               25%  {opacity: .8; left: -60px; bottom: 600px;}
               50%  {opacity: .5; left: -40px; bottom: 700px;}
               75%  {opacity: .8; left: 50px; bottom: 800px;}
               100% {opacity: .4; left: 100px; bottom: 950px;}
             }






          }

          /* Extra large devices (large laptops and desktops, 1200px and up) */
          @media only screen and (min-width: 1200px) {

            html, body {
             background-color: #fff;
             color: #636b6f;
             font-family: 'Buda', cursive;
             font-weight: 200;
             height: 100vh;
             margin: 0;
             background: 
                  linear-gradient(to right,rgba(240, 200, 180, 0.9), rgba(70, 50, 20, 0.7)),
                  url('https://ipdatamanager.s3.amazonaws.com/images/pexels-nextvoyage-3053859+(1).jpg');
              
              background-size: 120%;
             }

           

             .full-height {
             height: 100vh;
             }

             .flex-center {
             align-items: center;
             display: flex;
             justify-content: center;
             }

             .position-ref {
             position: relative;
             }

             .top-right {
             position: absolute;
             right: 160px;
             top: 18px;
             }

             .content {
               text-align: center;
             }

             .tagline {

               font-size: 1.2em;
               color: #FAF6F6 ;
               text-shadow: 2px 2px 2px  #0150A2 ;
               position: relative;

             }

             .title {
             font-size: 5.2rem;
             color: #FAF6F6 ;
             text-shadow: 5px 5px 5px #0150A2 ;
             position: relative;
             animation-name: title;
             animation-duration: 5s;
             animation-iteration-count: infinite;
             animation-direction: alternate;
             animation-timing-function: ease;

             }

             @keyframes title{
               0%   {text-shadow: 6px 6px 6px  #930031; }
               25%  {text-shadow: 6px 6px 6px  #0150A2; }
               50%  {text-shadow: 6px 6px 6px #EB365C; }
               75%  {text-shadow: 6px 6px 6px  #00B0D2; }
               100% {text-shadow: 6px 6px 6px  #930031; }
             }



             .links > a {
             color: #FAF6F6;
             padding: 0 25px;
             font-size: 1em;
             font-weight: 600;
             letter-spacing: .1rem;
             text-decoration: none;
             text-transform: uppercase;
             text-shadow: 2px 2px 2px #0150A2;
             }

             .m-b-md {
             margin-bottom: 30px;
             }

             .firefly1 {

             color: #00B0D2;
             font-size: 16em;
             opacity: .9;
             position: relative;
             animation-name: s;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes s{
               0%   {opacity: .2; left: 300px; bottom: 300px; }
               25%  {opacity: .3; left: 250px; bottom: 500px;}
               50%  {opacity: .5; left: 200px; bottom: 750px;}
               75%  {opacity: .4; left: 100px; bottom: 800px;}
               100% {opacity: .2; left: 10px; bottom: 900px;}
             }

             .firefly2 {

             color: #FEE7A4;
             font-size: 12em;
             opacity: .7;
             position: relative;
             animation-name: g;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes g{
               0%   {opacity: .2; left: 600px; bottom: 500px; }
               25%  {opacity: .3; left: 450px; bottom: 600px;}
               50%  {opacity: .4; left: 300px; bottom: 700px;}
               75%  {opacity: .3; left: 200px; bottom: 800px;}
               100% {opacity: .2; left: 70px; bottom: 900px;}
             }


             .firefly3 {

             color: #ED3B5D;
             font-size: 6em;
             opacity: .8;
             position: relative;
             animation-name: b;
             animation-duration: 13s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes b{
               0%   {opacity: .4; left: 650px; bottom: 750px; }
               25%  {opacity: .6; left: 750px; bottom: 850px;}
               50%  {opacity: .7; left: 800px; bottom: 900px;}
               75%  {opacity: .5; left: 900px; bottom: 950px;}
               100% {opacity: .3; left: 900px; bottom: 1000px;}
             }


             .firefly4 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .8;
             position: relative;
             animation-name: p;
             animation-duration: 20s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes p{
               0%   {opacity: .3; left: 300px; bottom: 550px; }
               25%  {opacity: .5; left: 400px; bottom: 650px;}
               50%  {opacity: .6; left: 500px; bottom: 750px;}
               75%  {opacity: .4; left: 700px; bottom: 800px;}
               100% {opacity: .3; left: 800px; bottom: 850px;}
             }


             .firefly5 {

             color: #EFE6DD;
             font-size: 2em;
             opacity: .7;
             position: relative;
             animation-name: q;
             animation-duration: 8s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes q{
               0%   {opacity: .3; left: 200px; bottom: 300px; }
               25%  {opacity: .5; left: 300px; bottom: 450px;}
               50%  {opacity: .6; left: 400px; bottom: 550px;}
               75%  {opacity: .4; left: 500px; bottom: 700px;}
               100% {opacity: .3; left: 700px; bottom: 850px;}
             }


             .firefly6 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: a;
             animation-duration: 14s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes a{
               0%   {opacity: .6; left: 10px; bottom: 400px; }
               25%  {opacity: .8; left: -100px; bottom: 550px;}
               50%  {opacity: .5; left: -300px; bottom: 650px;}
               75%  {opacity: .8; left: -400px; bottom: 800px;}
               100% {opacity: .4; left: -500px; bottom: 950px;}
             }


             .firefly7 {

             color: #EFE6DD;
             font-size: 2em;
             opacity: .7;
             position: relative;
             animation-name: levitate1;
             animation-duration: 14s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes levitate1{
               0%   {opacity: .6; left: -30px; bottom: 400px; }
               25%  {opacity: .8; left: -400px; bottom: 550px;}
               50%  {opacity: .5; left: -500px; bottom: 650px;}
               75%  {opacity: .8; left: -600px; bottom: 900px;}
               100% {opacity: .4; left: -550px; bottom: 950px;}
             }


              .firefly8 {

             color: #EFE6DD;
             font-size: 1em;
             opacity: .7;
             position: relative;
             animation-name: levitate2;
             animation-duration: 10s;
             animation-iteration-count: infinite;
             animation-timing-function: linear;

             }

             @keyframes levitate2{
               0%   {opacity: .6; left: -100px; bottom: 500px; }
               25%  {opacity: .8; left: -200px; bottom: 600px;}
               50%  {opacity: .5; left: -300px; bottom: 700px;}
               75%  {opacity: .8; left: -400px; bottom: 800px;}
               100% {opacity: .4; left: -50px; bottom: 950px;}
             }

           }

            
            

        </style>

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">
                         <i class="fa fa-home" style="font-size:24px; color: #FBD7B1;"></i>
                        </a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                   IPDataManager
                </div>

                <div class="tagline mx-auto">
                  <h4>An intellectual property data and asset management system for every creative enterprise.</h4>
                </div>

                <div class="links">

                  @guest
                    <a href="{{ route('login') }}">Login</a>
                 
                    <a href="{{ route('register') }}">Register</a>
                  @else
                    <h5 style="font-size:24px; color: #FBD7B1;">You are logged-in! Go to 
                     <a href="{{ url('/home') }}">
                         <i class="fa fa-home" style="font-size:24px; color: #FBD7B1;"></i>
                        </a>
                    </h5>
                  @endguest
                 
                </div>

                <span class="badge" style="background-color: #FE8676; padding: 5px; color: #FEEDDB; opacity: .5; border-radius: 5px; font-size: .5em; margin-top: 20px; font-family: 'Nunito', sans-serif;">
                 <b>&#169;</b> S.Cabrera 2020
               </span>

     
            </div>



        </div>



        <div class="mx-auto">


           <span class="badge firefly1"><i class="fa fa-cog fa-spin"></i></span>
           <span class="badge firefly2"><i class="fa fa-cog fa-spin"></i></span>
           
           <span class="badge firefly3"><i class="fa fa-cog fa-spin"></i></span>
           <span class="badge firefly4"><i class="fa fa-cog fa-spin"></i></span>
           <span class="badge firefly5"><i class="fa fa-cog fa-spin"></i></span>

           <span class="badge firefly6"><i class="fa fa-cog fa-spin"></i></span>
           <span class="badge firefly7"><i class="fa fa-cog fa-spin"></i></span>
           <span class="badge firefly8"><i class="fa fa-cog fa-spin"></i></span>

           
 
        </div>

       


    </body>
</html>
