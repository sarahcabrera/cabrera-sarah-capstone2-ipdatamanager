@extends('layouts.app')

@section('content')

	{{-- alert-message --}}
	@includeWhen(Session::has('message'),'partials.alert')
	
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-4">
				<img src="{{ $ip_license->ip_asset->image }}" alt="" class="w-100">


				<div class="row mt-3 text-justify">
					
					
						<div class="col">

							@php

								 $ip_asset = $ip_license->ip_asset->id ;

							@endphp

							 
								
								<a href="{{ route('ip_assets.update', $ip_asset) }}">
									<button class="btn-info w-100">View IP Asset</button>

								</a>
								
							
						</div>
					
				</div>
			</div>
		
			<div class="col-12 col-md-8">
				<h1>{{ $ip_license->ip_asset->name }}</h1>

				{{-- barcode start --}}
				<div class="row my-1">
					<div class="col-4">IP License Barcode</div>
					<div class="col">
						<span 
							  style="font-family: 'Libre Barcode 128', cursive; font-size: 2.5em;"
						>
							{{ $ip_license->code }}
						</span>
					</div>
				</div>
				{{-- barcode end --}}


				{{-- license start --}}
				<div class="row my-2">
					<div class="col-4">IP License Code</div>
					<div class="col">
						<span >
							{{ $ip_license->code }}
						</span>
					</div>
				</div>
				{{-- license end --}}

				{{-- license start --}}
				<div class="row my-2">
					<div class="col-4">IP License Duration in Months</div>
					<div class="col">
						<span >
							{{ $ip_license->duration_in_months }}
						</span>
					</div>
				</div>
				{{-- license end --}}

				{{-- license status start --}}
				<div class="row my-2">
					<div class="col-4">IP License Status</div>
					<div class="col">
						<span >
							{{ $ip_license->ip_license_status->name }}
						</span>
					</div>
				</div>
				{{-- license status end --}}
				@cannot('isAdmin')
					@if($ip_license->ip_license_status->id === 1)
						{{-- Add Request start --}}
						<div class="row my-2">
							<div class="col-4"> Request</div>
							<div class="col">
								@include('ip_licenses.partials.add-request')
							</div>
						</div>
						{{-- Add Request end --}}
					@endif
				@endcannot
				

				
				@can('isAdmin')
				{{-- admin buttons start --}}
				<div class="row">
					<div class="col-4">Edit | Delete</div>
					{{-- edit start --}}
					<div class="col col-md-4">
						@include('ip_licenses.partials.edit-btn')
					</div>
					{{-- edit end --}}

					{{-- delete start --}}
					<div class="col col-md-4">
						@include('ip_licenses.partials.delete-form')
					</div>
					{{-- delete end --}}
				</div>
				{{-- admin buttons end --}}
				@endcan



				

			</div>
		</div>
	</div>

@endsection