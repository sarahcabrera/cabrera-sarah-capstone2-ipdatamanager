@extends('layouts.app')

@section('content')

	{{-- alert-message --}}
	@includeWhen(Session::has('message'),'partials.alert')

	
	<div class="container container-fluid">
		{{-- header start --}}
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					IP Assets Licenses
				</h1>
			</div>
		</div>
		{{-- header end --}}

		{{-- ip_licenses section start --}}
		<div class="row">
			@foreach($ip_licenses as $ip_license)
				<div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-3 mx-auto">
					{{-- ip_license card start --}}
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">
								{{ $ip_license->ip_asset->name }}
							</h5>
							<h6 class="card-title">
								{{ $ip_license->code }}
							</h6>

							

							<p class="card-text my-1">
								<span 
							  			style="font-family: 'Libre Barcode 128', cursive; font-size: 2em; "
								>
									{{ $ip_license->code }}
								</span>
								
							</p>

							<p class="card-text my-1">


								
								<span class="badge badge-light">
									{{ $ip_license->ip_license_status->name }}
								</span>

								<span class="badge badge-light">
									{{ $ip_license->duration_in_months }} month(s)
								</span>
							</p>


							<a href="{{ route('ip_licenses.show', $ip_license->id) }}" class="btn btn-sm btn-info w-100">
								View
							</a>
						</div>
						
						@can('isAdmin')
							<div class="card-footer">

								@include('ip_licenses.partials.edit-btn')
								@include('ip_licenses.partials.delete-form')
							
							</div>
						@endcan
						
					</div>

					{{-- ip_license card end --}}
				</div>
			@endforeach
		</div>
		{{-- end of row --}}

		{{-- ip_licenses section end --}}
	</div>
@endsection