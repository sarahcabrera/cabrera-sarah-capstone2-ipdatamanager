@extends('layouts.app')


@section('content')


	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif




	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 col-lg-6 mx-auto">
				<h1 class="text-center">Edit IP License</h1>
			</div>
		</div>

		{{-- edit form start --}}
		<div class="row">
			<div class="col-12 col-md-8 col-lg-6 mx-auto">
				<form 
					action="{{ route('ip_licenses.update', $ip_license->id)}}"
					method="post" 
					
				>
					@csrf
					@method('PUT')


					{{-- start ip_asset_id --}}
					<label for="ip_asset_id" class="mt-2">IP Asset Category</label>
					<select 
							name="ip_asset_id" 
							id="ip_asset_id" 
							class="form-control form-control-sm
											@error('ip_asset_id') 
												is-invalid 
											@enderror"
					>
						@foreach($ip_assets as $ip_asset)
							<option 
								value="{{$ip_asset->id}}"
								{{ $ip_asset->id === $ip_license->ip_asset_id ? "selected" : "" }}
							>{{$ip_asset->name}}</option>
						@endforeach
					</select>
					@error('ip_id')
						<small class="d-block invalid-feedback">
							<strong>							
								{{ $message }}
							</strong>
						</small>
					@enderror
					{{-- end ip_asset_id --}}


					{{-- duration --}}
					 @include('ip_licenses.partials.form-group', [

									'name' => 'duration_in_months',
									'type' => 'number',
									'classes' => ['form-control', 'form-control-sm']
									

						])


					 {{-- ip_license_status_id --}}
					<label for="ip_license_status_id" class="mt-2">IP Asset Category</label>
					<select 
							name="ip_license_status_id" 
							id="ip_license_status_id" 
							class="form-control form-control-sm
											@error('ip_license_status_id') 
												is-invalid 
											@enderror"
					>
						@foreach($ip_license_statuses as $ip_license_status)
							<option 
								value="{{$ip_license_status->id}}"
								{{ $ip_license_status->id === $ip_license->ip_license_status_id ? "selected" : "" }}
							>{{$ip_license_status->name}}</option>
						@endforeach
					</select>
						@error('ip_id')
							<small class="d-block invalid-feedback">
								<strong>							
									{{ $message }}
								</strong>
							</small>
						@enderror
					{{-- end ip_license_status_id --}}



					<button class="btn btn-sm btn-warning my-2">Edit IP License</button>
					

				</form>
				
				<br>

				
			</div>
		</div>
		{{-- edit form end --}}
	</div>

@endsection



