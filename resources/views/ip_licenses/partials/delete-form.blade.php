<form action="{{ route('ip_licenses.destroy', $ip_license->id )}}" method="post">
	@csrf
	@method('DELETE')
	<button class="btn btn-sm btn-danger w-100 mb-1">
		Delete
	</button>
</form>