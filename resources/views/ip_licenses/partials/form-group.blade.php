<div class="form-group">
	<label for="{{ $name }}" class="mt-2">
		
		IP Asset {{ Str::replaceLast('_', ' ', (Str::replaceFirst('_', ' ', Str::title($name)))) }}:

	
	</label>
	<input 
			type="{{ $type }}" 
			name="{{ $name }}" 
			id="{{ $name }}" 
			class="
				form-control 

				form-control-sm

				

				@error($name) 
					is-invalid 
				@enderror

				" 
			autofocus
			value="{{ isset($value) ? $value : '' }}"
			
	>

	@error($name)
		<small class="d-block invalid-feedback">
			<strong>
				{{ $message }}
			</strong>
		</small>
	@enderror
	
</div>