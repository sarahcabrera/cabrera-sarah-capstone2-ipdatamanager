@extends('layouts.app')

@section('content')

	@if($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
 



	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Add IP License
				</h1>
			</div>
		</div>

		{{-- add ip_asset section start --}}
		<div class="row">
			<div class="col-12 col-md-10 col-lg-6 mx-auto">
				
				{{-- add ip_asset form start --}}
				<form 
					action="{{ route('ip_licenses.store')}}" 
					method="post"
					enctype="multipart/form-data" 
				>
					@csrf

					 {{-- ip asset code --}}

						
							<div class="form-group my-2 w-100">
									<div class="input-group mb-3">
											  <div class="input-group-prepend">
											    <span class="input-group-text" id="inputGroup-sizing-default">

											    	IP License Code

															</span>
											  </div>

											  @php
													$randomCode = Str::upper(Str::random(16));
											  @endphp
											  <input 
													type="text" 
													name="code" 
													id="code" 
													class=" form-control form-control-default

														@error('code') 
															is-invalid 
														@enderror

														" 
													placeholder="{{ $randomCode }}" 
													value="{{ $randomCode }}"
													autofocus
													required
													aria-label="code" 
													aria-describedby="inputGroup-sizing-default"
												>
									</div>
							</div>

					


					 {{-- ip_asset_id --}}
					 <label for="ip_asset_id" class="mt-2">IP Asset:</label>
					 <select name="ip_asset_id" id="ip_asset_id" class="form-control form-control-sm mb-2">

					 		@foreach($ip_assets as $ip_asset)
					 			<option value="{{ $ip_asset->id }}">
					 				{{ $ip_asset->name }}
					 			</option>
							@endforeach
							
					 </select>


					  {{-- duration --}}
					 @include('ip_assets.partials.form-group', [

									'name' => 'duration_in_months',
									'type' => 'number',
									'classes' => ['form-control', 'form-control-sm']
									

						])


					 {{-- ip_license_status_id --}}
					 <label for="ip_license_status_id" class="mt-2">IP License Status:</label>
					 <select name="ip_license_status_id" id="ip_license_status_id" class="form-control form-control-sm">

					 		@foreach($ip_license_statuses as $ip_license_status)
					 			<option value="{{ $ip_license_status->id }}">
					 				{{ $ip_license_status->name }}
					 			</option>
							@endforeach
					 </select>

					 
					 <button class="btn btn-sm btn-primary mt-3">Add IP Asset</button>

					
				</form>
				{{-- add ip_asset form end --}}
			</div>
		</div>
		{{-- add ip_asset section end --}}
	</div>



@endsection