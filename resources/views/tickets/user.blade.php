@extends('layouts.app')
@section('content')

	<div class="container col-8">

		<div class="row mb-2">
			<div class="col">
				<h1 class="text-center">
					All Request Tickets
				</h1>
			</div>
		</div>

	{{-- alert-message --}}
  @includeWhen(Session::has('message'),'partials.alert')


  		<div class="row">
  			<div class="col">
  				<nav class="navbar navbar-expand-lg navbar-dark bg-info">
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>
				  <a class="navbar-brand" href="{{ route('tickets.index')}}">All Tickets</a>

				  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
				     <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
				      <li class="nav-item ">
				        <a class="nav-link
				        									{{ Route::CurrentRouteNamed('tickets.pending') ? "active" : "" }}

				        " href="{{ route('tickets.pending')}}">Pending</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link
				        								{{ Route::CurrentRouteNamed('tickets.approved') ? "active" : "" }}

				        " href="{{ route('tickets.approved')}}">Approved</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link
				        									{{ Route::CurrentRouteNamed('tickets.declined') ? "active" : "" }}

				        " href="{{ route('tickets.declined')}}">Declined</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link

				        							{{ Route::CurrentRouteNamed('tickets.completed') ? "active" : "" }}

				        " href="{{ route('tickets.completed')}}">Completed</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link
				        								{{ Route::CurrentRouteNamed('tickets.recent') ? "active" : "" }}

				        " href="{{ route('tickets.recent')}}">Most Recent First</a>
				      </li>
				      @can('isAdmin')
				      <li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle
				        								{{ Route::CurrentRouteNamed('tickets.user') ? "active" : "" }}

				        " href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          Filter By User
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
				        	@foreach($users as $user)
				          <a class="dropdown-item" href="{{ route('tickets.user', $user->id )}}">{{ $user->name }}</a>
				         @endforeach
				        </div>
				      </li>
				      @endcan
				    </ul>
				   
				  </div>
				</nav>
  			</div>
  		</div>

		
		<div class="row mt-2">

			<div class="col-12">
					@include('tickets.partials.all-tickets')
			</div>
	
		</div>
		{{-- end of row --}}
	</div>
@endsection