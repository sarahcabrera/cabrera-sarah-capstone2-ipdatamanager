<form action="{{ route('tickets.update', $ticket->id )}}" method="post" class=" border p-3">
	@csrf
	@method("PUT")
	<label for="status_id">Edit:</label>
	<select name="status_id" id="status_id" class="form-control form-control-sm">
		@foreach($ticket_statuses as $ticket_status)
			<option 
				value="{{ $ticket_status->id }}"

				{{ $ticket_status->id === $ticket->status_id ? "selected" : "" }}

			>

				{{ $ticket_status->name }}

			</option>
		@endforeach
	</select>
	<button class="btn btn-sm btn-outline-primary my-1">Edit Status</button>
</form>