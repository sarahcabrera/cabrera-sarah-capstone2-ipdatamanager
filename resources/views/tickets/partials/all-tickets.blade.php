
@foreach($tickets as $ticket)
					{{-- tickets section start --}}
						<div class="row">
							<div class="col">
									<div class="accordion" id="accordionExample">

										  {{-- ticket card start --}}
										  
										  <div class="card">
										    <div class="card-header" id="heading{{$ticket->id}}">
										      <h2 class="mb-0">
										        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse{{$ticket->id}}" aria-expanded="true" aria-controls="collapse{{$ticket->id}}">
										          {{ $ticket->ticket_code }} | 
										          <span class="text-dark"> 

										          	{{ Str::upper($ticket->user->name) }} |

										          </span>
										          {{ date("M d, Y (h:i A)", strtotime($ticket->created_at)) }} |
										          <span class="badge 
										             badge-{{ $ticket->status_id === 1 ? "warning" : ($ticket->status_id == 2 ? "success" : ($ticket->status_id == 3 ? "danger" : "info" ) )}}
										                      "
										          >
										           {{ $ticket->status_id === 1 ? "Pending" : ($ticket->status_id == 2 ? "Approved" : ($ticket->status_id == 3 ? "Denied" : "Completed" ) )}}
										          </span>
										        </button>
										      </h2>
										    </div>

										    <div id="collapse{{$ticket->id}}" class="collapse hidden" aria-labelledby="heading{{$ticket->id}}" data-parent="#accordionExample">
										      <div class="card-body">
										        
										          

										          {{-- table start --}}
										            <div class="table-responsive">
										              <table class="table table-hover">
										                {{-- transacation code --}}
										                <tr>
										                  <td>Ticket Code</td>
										                  <td>{{ $ticket->ticket_code }}</td>
										                </tr>
										                {{-- ticket code end --}}
										                
										                {{-- Customer name start --}}
										                <tr>
										                  <td>Customer</td>
										                  <td>{{ $ticket->user->name }}</td>
										                </tr>
										                {{-- Customer name end --}}

										                {{-- Date start --}}
										                <tr>
										                  <td>Request Date</td>
										                  <td>{{ date("M d Y", strtotime($ticket->created_at)) }}</td>
										                </tr>
										                {{-- Date end --}}

										                {{-- Status start --}}
										                <tr>
										                  <td>Status</td>
										                  <td>
										                    <span class="badge 
																					             badge-{{ $ticket->status_id === 1 ? "warning" : ($ticket->status_id == 2 ? "success" : ($ticket->status_id == 3 ? "danger" : "info" ) )}}
																					                      mb-2"
																					          >
																					           {{ $ticket->status_id === 1 ? "Pending" : ($ticket->status_id == 2 ? "Approved" : ($ticket->status_id == 3 ? "Denied" : "Completed" ) )}}
																					          </span>

																					         @can('isAdmin') 
										                    				@include('tickets.partials.edit-status')


										                    				<h6 class="mt-2">After ticket status change, click the button(s) below to update the IP Asset Inventory. </h6>
							
										                    @endcan
										                  </td>
										                </tr>
										                {{-- Status end --}}

										              </table>
										            </div>
										                  {{-- table end --}}

										        
																							<div class="row">
																									<div class="col-12">
																										{{-- table start --}}
																										<div class="table-responsive">
																											<table class="table table-hover">
																												<thead>
																													<tr>
																														<th>Requested Asset</th>
																														<th>License Status</th>
																														<th>Duration</th>
																														<th>Date Needed</th>
																														<th>Return Date</th>

																														@can('isAdmin')
																																<th>Update Inventory</th>
																														@endcan

																													</tr>
																												</thead>
																												<tbody>

																													
																													@foreach($ticket->ip_licenses as $ip_license)

																																{{-- ip_license row start --}}
																																<tr>
																																	<td>{{ $ip_license->ip_asset->name }}</td>
																																	{{-- <td>{{ $ip_license->ip_asset->code }}</td>
																																	<td>{{ $ip_license->code }}</td> --}}
																																	<td>{{-- {{ $ip_license->ip_license_status->name }} --}}
																																				
																																				<span class="badge 
																															             badge-{{ $ip_license->ip_license_status_id === 1 ? "success" : ($ip_license->ip_license_status_id == 2 ? "danger" : "" ) }}
																															                      mb-2"  >
																															          	{{ $ip_license->ip_license_status_id === 1 ? "Available" : ($ip_license->ip_license_status_id == 2 ? "Unavailable" : "" ) }}
																															     </span>


																																	</td>
																																	<td>{{ $ip_license->pivot->duration_in_months }} month(s)</td>
																																	<td>{{ date('M d Y', strtotime($ip_license->pivot->date_needed) ) }}</td>
																																	<td>{{ date('M d Y', strtotime($ip_license->pivot->return_date) ) }}</td>
																																	
																																				@can('isAdmin')
																																						<td>
																																							<button name="update_inventory_db" 
																																							    		class="btn btn-sm btn-warning btn-outline-primary">
																																									<a 
																																									   href="{{ route('ip_assets.edit', $ip_license->ip_asset->id) }}">
																																										Update 
																																									</a>
																																								</button>
																																						</td>
																																				@endcan


																																</tr>
																																{{-- ip_license row end --}}
																										

																													@endforeach
																													
																												</tbody>
																												
																											</table>
																										</div>
																										{{-- table end --}}


																						<a classs="my-2" 
																									href="{{ route('tickets.show', $ticket->id )}}}">View details</a>


										      </div>
										    </div>
										  </div>
										  {{-- ticket card start --}}
										</div>
							</div>
						</div>
					{{-- tickets section end --}}
@endforeach
