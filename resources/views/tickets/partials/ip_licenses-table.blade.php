<div class="row">
	<div class="col-12">
		{{-- table start --}}
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Requested Asset</th>
						<th>Asset Code</th>
						<th>License Code</th>
						<th>License Status</th>
						<th>Duration</th>
						<th>Date Needed</th>
						<th>Return Date</th>
					</tr>
				</thead>
				<tbody>

					
					@foreach($ticket->ip_licenses as $ip_license)
						{{-- ip_license row start --}}
						<tr>
							<td>

									<a href="{{ route('ip_assets.show', $ip_license->ip_asset->id ) }}">
											{{ $ip_license->ip_asset->name }}
									</a>


							</td>
							<td>{{ $ip_license->ip_asset->code }}</td>

							<td>
									<a href="{{ route('ip_licenses.show', $ip_license->id ) }}">
											{{ $ip_license->code }}
									</a>
							</td>
							<td>

								<span class="badge 
			             badge-{{ $ip_license->ip_license_status_id === 1 ? "success" : ($ip_license->ip_license_status_id == 2 ? "danger" : "" ) }}
			             mb-2"  >
			          	{{ $ip_license->ip_license_status_id === 1 ? "Available" : ($ip_license->ip_license_status_id == 2 ? "Unavailable" : "" ) }}
			     </span>

							</td>
							<td>{{ $ip_license->pivot->duration_in_months }} month(s)</td>
							<td>{{ date('M d Y', strtotime($ip_license->pivot->date_needed) ) }}</td>
							<td>{{ date('M d Y', strtotime($ip_license->pivot->return_date) ) }}</td>
						</tr>
						{{-- ip_license row end --}}
					@endforeach
					
				</tbody>
				
			</table>
		</div>
		{{-- table end --}}
	</div>
</div>