@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12 mx-auto">
			<h1 class="text-center"> 

				<span class="badge badge-large text-center p-3 mx-auto
	             badge-{{ $ticket->status_id === 1 ? "warning" : ($ticket->status_id == 2 ? "success" : ($ticket->status_id == 3 ? "danger" : "info" ) )}}
	                      mb-2"
	          >
	           {{ $ticket->status_id === 1 ? "Pending" : ($ticket->status_id == 2 ? "Approved" : ($ticket->status_id == 3 ? "Denied" : "Completed" ) )}}
	         </span>

			Request Ticket</h1>
			<br>


			
		</div>
	</div>

	{{-- ticket section start --}}
	<div class="row">
		<div class="col-12">
			@include('tickets.partials.ip_licenses-table')
		</div>
	</div>
	
	{{-- ticket section end --}}

	<div class="row mt-2">
		<div class="col-10">
			
		</div>
		<div class="col-2">
			<button class="btn-light btn-outline-success" style="border-radius: 5px;">
				<a href="{{ route('tickets.index') }}">Back to Tickets</a>
			</button>
		</div>
	</div>
</div>
@endsection
