@extends('layouts.app')
@section('content')

	<div class="container col-8">

		<div class="row mb-2">
			<div class="col">
				<h1 class="text-center">
					All Request Tickets
				</h1>
			</div>
		</div>

	{{-- alert-message --}}
  @includeWhen(Session::has('message'),'partials.alert')


  		<div class="row">
  			<div class="col">
  				<nav class="navbar navbar-expand-lg navbar-light"
  					style="font-size: 1rem; 
  					       font-family: 'Nunito', sans-serif;
  					       border-radius: 4px; 
  					       background: linear-gradient(to top, rgba(220,210,190,.95), rgba(200,120,120,.99));"

  				>
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>
				  <a class="navbar-brand" href="{{ route('tickets.index')}}">

				  	<i class="fa fa-sort-numeric-asc" aria-hidden="true"></i>
				  	All Tickets


				  </a>

				  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
				    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
				    	<li class="nav-item">
				        <a class="nav-link
				        								{{ Route::CurrentRouteNamed('tickets.recent') ? "active" : "" }}

				        " href="{{ route('tickets.recent')}}">

				        <i class="fa fa-sort-numeric-desc" aria-hidden="true"></i>
				        	Recent First

				    	</a>
				      </li>
				      <li class="nav-item ">
				        <a class="nav-link
				        									{{ Route::CurrentRouteNamed('tickets.pending') ? "active" : "" }}

				        " href="{{ route('tickets.pending')}}">
				        	<i class="fa fa-tasks" aria-hidden="true"></i>
				    		Pending
				    	</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link
				        								{{ Route::CurrentRouteNamed('tickets.approved') ? "active" : "" }}

				        " href="{{ route('tickets.approved')}}">

				        	<i class="fa fa-check-square-o" aria-hidden="true"></i>

				        	Approved

				    	</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link
				        									{{ Route::CurrentRouteNamed('tickets.declined') ? "active" : "" }}

				        " href="{{ route('tickets.declined')}}">


				        	<i class="fa fa-ban" aria-hidden="true"></i>
				        	Declined


				    	</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link

				        							{{ Route::CurrentRouteNamed('tickets.completed') ? "active" : "" }}

				        " href="{{ route('tickets.completed')}}">


				        	<i class="fa fa-handshake-o" aria-hidden="true"></i>
				        	Completed


				    	</a>
				      </li>
				      
				      @can('isAdmin')
				      <li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle
				        								{{ Route::CurrentRouteNamed('tickets.user') ? "active" : "" }}

				        " href="#" id="navbarDropdownMenuLink" 
				        role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

				        	<i class="fa fa-users" aria-hidden="true"></i>
				          	Filter By Users
				        </a>
				        <div class="dropdown-menu" 
				            style="font-size: 1rem; 
				            		border: 1px solid oldlace; 
				            		border-radius: 4px; 
				            		background: linear-gradient(to top, rgba(220,210,190,.95), rgba(200,120,120,.6));"

				            aria-labelledby="navbarDropdownMenuLink">
				        	@foreach($users as $user)
				          <a class="dropdown-item" href="{{ route('tickets.user', $user->id )}}">

				          	
				          	<i class="fa fa-user-circle" aria-hidden="true"></i>
				          	{{ $user->name }} 


				          </a>
				         @endforeach
				        </div>
				      </li>
				      @endcan
				    </ul>
				   
				  </div>
				</nav>
  			</div>
  		</div>

		
		<div class="row mt-2">

			<div class="col-12">
					@include('tickets.partials.all-tickets')
			</div>
	
		</div>
		{{-- end of row --}}
	</div>
@endsection