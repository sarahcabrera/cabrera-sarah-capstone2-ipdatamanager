<div class="row">
	<div class="col-6 p-2 w-100 my-2 mx-auto" 
		 style="font-size: 1.3rem;
	               ">
		<div class="alert 
					alert-{{ Session::has('alert') ? Session::get('alert') : "info" }}
					alert-dismissible fade show ">

				<div class="row text-justify py-2 px-4">
					
					<i class="fa fa-cog fa-spin text-success" style="font-size:44px"></i>
					<i class="fa fa-cog fa-spin text-warning" style="font-size:30px"></i>
					<strong  class="pt-1 ml-5">
						{{ Session::get('message') }}
					</strong>
					
				</div>
			
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
		</div>
	</div>
</div>


