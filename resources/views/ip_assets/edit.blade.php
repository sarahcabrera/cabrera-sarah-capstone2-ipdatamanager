@extends('layouts.app')


@section('content')


	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif

	


	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 col-lg-6 mx-auto">
				<h1 class="text-center">Update Inventory or Edit IP Asset</h1>
				<h2 class="text-center">{{ $ip_asset->name }}</h2>
			</div>
		</div>

		{{-- edit form start --}}
		<div class="row">
			<div class="col-12 col-md-8 col-lg-6 mx-auto">
				<form 
					action="{{ route('ip_assets.update', $ip_asset->id) }}"
					method="post" 
					enctype="multipart/form-data" 
				>
					@csrf
					@method('PUT')
				
				
					<button name="update_inventory_db"
							class="btn btn-warning btn-outline-primary mx-auto w-100 p-2 mt-3">

							Update IP Asset Inventory 

					</button>

					<!-- Button trigger modal -->
					<button type="button" class="btn btn-light btn-outline-danger mx-auto w-100 p-2 mt-3" data-toggle="modal" data-target="#exampleModalScrollable">
					  Edit IP Asset 
					</button>

					<!-- Modal -->
					<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
					  <div class="modal-dialog modal-dialog-scrollable" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalScrollableTitle">Edit IP Asset {{ $ip_asset->name }} </h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					        	
					        	{{-- name --}}
								@include('ip_assets.partials.form-group',[
									'name' => 'name',
									'type' => 'text',
									'value' => $ip_asset->name
									
								])


								{{-- creator --}}
								@include('ip_assets.partials.form-group',[
									'name' => 'creator',
									'type' => 'text',
									'value' => $ip_asset->creator
									
								])

								

								{{-- image --}}
								@include('ip_assets.partials.form-group',[
									'name' => 'image',
									'type' => 'file',
									
								])


								{{-- start ip_id --}}
								<label for="ip_id" class="mt-2">IP Asset Category</label>
								<select 
										name="ip_id" 
										id="ip_id" 
										class="form-control form-control-sm
														@error('ip_id') 
															is-invalid 
														@enderror"
								>
									@foreach($ips as $ip)
										<option 
											value="{{$ip->id}}"
											{{ $ip->id === $ip_asset->ip_id ? "selected" : "" }}
										>{{$ip->name}}</option>
									@endforeach
								</select>
								@error('ip_id')
									<small class="d-block invalid-feedback">
										<strong>							
											{{ $message }}
										</strong>
									</small>
								@enderror
								{{-- end ip_id --}}


								{{-- start status_id --}}
								<label for="status_id" class="mt-2">IP Asset Status:</label>
								<select 
												name="status_id" 
												id="status_id" 
												class="form-control form-control-sm
														@error('status_id') 
															is-invalid 
														@enderror"
								>


									@foreach($statuses as $status)
										<option 
											value="{{$status->id}}"
											{{ $status->id === $ip_asset->status_id ? "selected" : "" }}
										>{{$status->name}}</option>
									@endforeach
								</select>
								@error('status_id')
									<small class="d-block invalid-feedback">
										<strong>							
											{{ $message }}
										</strong>
									</small>
								@enderror
								{{-- end status_id --}}


								{{-- start type_id --}}
								<label for="type_id" class="mt-2">IP Asset Type:</label>
								<select 
												name="type_id" 
												id="type_id" 
												class="form-control form-control-sm

														@error('type_id') 
															is-invalid 
														@enderror"
								>

									@foreach($types as $type)
										<option 
											value="{{$type->id}}"
											{{ $type->id === $ip_asset->type_id ? "selected" : "" }}
										>{{$type->name}}</option>
									@endforeach
								</select>
								@error('type_id')
									<small class="d-block invalid-feedback">
										<strong>							
											{{ $message }}
										</strong>
									</small>
								@enderror
								{{-- end type_id --}}


								{{-- created_date --}}
								@include('ip_assets.partials.form-group',[
									'name' => 'creation_date',
									'type' => 'date',
									
									
								])

								{{-- registered_date --}}
								@include('ip_assets.partials.form-group',[
									'name' => 'registered_date',
									'type' => 'date',
									
									
								])

								{{-- renewal_due_date --}}
								@include('ip_assets.partials.form-group',[
									'name' => 'renewal_due_date',
									'type' => 'date'
									
									
								])

								

								{{-- start renewal_status_id --}}
								<label for="renewal_status_id" class="mt-2">IP Asset renewal_status:</label>
								<select 
											name="renewal_status_id" 
											id="renewal_status_id" 

											class="form-control form-control-sm mb-2
														@error('renewal_status_id') 
															is-invalid 
														@enderror"
								>

									@foreach($renewal_statuses as $renewal_status)
										<option 
											value="{{$renewal_status->id}}"
											{{ $renewal_status->id === $ip_asset->renewal_status_id ? "selected" : "" }}
										>{{$renewal_status->name}}</option>
									@endforeach
								</select>
								@error('renewal_status_id')
									<small class="d-block invalid-feedback">
										<strong>							
											{{ $message }}
										</strong>
									</small>
								@enderror
								{{-- end renewal_status_id --}}



								{{-- start description --}}
								<label for="description">IP Asset Description</label>
								<textarea 
													name="description" 
													id="description" 
													cols="30" 
													rows="10" 
													class="
																		form-control 
																		form-control-sm
																		@error('description') 
																			is-invalid 
																		@enderror
																		"
									>
									{{ $ip_asset->description }}
								</textarea>

								
										@error('description')
											<small class="d-block invalid-feedback">
												<strong>							
													{{ $message }}
												</strong>
											</small>
										@enderror
								{{-- end description --}}

								{{-- <button class="btn btn-sm btn-warning my-2">Edit IP Asset </button> --}}
					      	</div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					        <button type="submit" class="btn btn-warning">Edit IP Asset</button>
					      </div>
					    </div>
					  </div>
					</div>

				</form>
			</div>
		</div>
		{{-- edit form end --}}
	</div>

@endsection



