<form action="{{ route('ip_assets.destroy', $ip_asset->id )}}" method="post">
	@csrf
	@method('DELETE')
	<button class="btn btn-sm btn-danger w-100 mb-1">
		Delete
	</button>
</form>