@extends('layouts.app')

@section('content')

	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif

	{{-- alert-message --}}
	@includeWhen(Session::has('message'),'partials.alert')

	<div class="container">
		<div class="row">

			<div class="col-11 col-md-12  col-md-8 col-lg-8 mx-auto p-3 px-4"

				style="border: 1px solid lightsalmon; border-radius: 10px; background-color: #FEEDDB" 
			>

				<div class="row">
					<div class="col">
						<h1 class="my-2">{{ $ip_asset->name }}</h1>
					</div>
				</div>

				<div class="row">

					<div class="col-12 col-md-8 col-lg-8 my-2 py-2">
							
								<p class="text-justify my-2" style="font-size: 1.1em;">
										{{ $ip_asset->description }}
								</p>
					</div>

					<div class="col-12 col-lg-4 my-3">
							<img src="{{ $ip_asset->image }}" alt="" 
								class="w-100"
								style=" border-radius: 5px;" 

								>


					</div>

					

				</div>
				

				{{-- creator start --}}
				<div class="row mt-3 mb-2">
					<div class="col-4 ">Creator</div>
					<div class="col text-justify">{{ $ip_asset->creator }}</div>
				</div>
				{{-- creator end --}}

				{{-- asset barcode start --}}
				<div class="row my-1">
					<div class="col-4 ">IP Asset Barcode</div>
					<div class="col">
						<span 
							  style="font-family: 'Libre Barcode 128', cursive; font-size: 2.5em;"
						>
							{{ $ip_asset->code }}
						</span>
					</div>
				</div>
				{{-- asset barcode end --}}


				{{-- asset code start --}}
				<div class="row my-2">
					<div class="col-4 ">IP Asset Code</div>
					<div class="col">
						<span >
							{{ $ip_asset->code }}
						</span>
					</div>
				</div>
				{{-- asset code end --}}

				{{-- category  & type start --}}
				<div class="row my-2">
					<div class="col-4 ">IP Category | Asset Type | Status </div>
					<div class="col">
						<span class="badge 
									badge-{{ $ip_asset->ip->id == 1 ? "warning" : ($ip_asset->ip->id == 2 ? "info" : "danger" ) }}
									">
							{{ $ip_asset->ip->name }}
						</span>
						<span class="badge badge-success">
							{{ $ip_asset->type->name }}
						</span>

						<span class="badge badge-light">
							{{ $ip_asset->status->name }}
						</span>

					</div>
				</div>
				{{-- category  & type end --}}
				
				@can('isAdmin')
				{{-- protection term --}}
				<div class="row my-2">
					<div class="col-4 ">Protection Term from {{ $ip_asset->ip->name}}</div>
					<div class="col">
							<h6 class="text-justify">
								{{ $ip_asset->ip->term }}
							</h6>
					</div>
				</div>
				{{-- protection term  --}}


				{{-- protection start --}}
				<div class="row my-2">
					<div class="col-4 ">Protection Start of {{ $ip_asset->ip->name}}</div>
					<div class="col">
							<h6 class="text-justify">
								{{ $ip_asset->ip->protection_start }}
							</h6>
					</div>
				</div>
				{{-- protection start  --}}

				{{-- creation date start --}}
				<div class="row my-2">
					<div class="col-4 ">Creation Date</div>
					<div class="col text-justify">{{ date('M d Y', strtotime($ip_asset->creation_date))}}</div>
				</div>
				{{-- creation date end --}}

				{{-- registered date start --}}
				<div class="row my-2 ">
					<div class="col-4">Registration Date</div>
					<div class="col text-justify">{{ date('M d Y', strtotime($ip_asset->registered_date)) }}</div>
				</div>
				{{-- registered date end --}}
				

				
				{{-- renewal_status date start --}}
				<div class="row my-2">
					<div class="col-4 ">Renewal Status</div>
					<div class="col">
						<span >
							{{ $ip_asset->renewal_status->name }}
						</span>
					</div>
				</div>
				{{-- renewal_status end --}}

				
				{{-- renewal_due_date  start --}}
				<div class="row my-2">
					<div class="col-4 ">Renewal Due Date</div>
					<div class="col text-justify">{{ date('M d Y', strtotime($ip_asset->renewal_due_date)) }}</div>
				</div>
				{{-- renewal_due_date end --}}

				
				

				{{-- license_limit start --}}
				<div class="row my-2 ">
					<div class="col-4 ">License Limit</div>
					<div class="col-1">
						<span >
							{{ $ip_asset->license_limit }}
						</span>
					</div>
					<div class="col">
						
							<h6 class="text-justify p-3 "
								style="border: 1px solid skyblue;">

							The <b>License Limit</b>, <b>Quanity Licensed</b>, and <b>Available Inventory</b> are <b>automatically updated</b> by Adding, Editing, or Deleting  <a href="{{ route('ip_licenses.index') }}">IP Licenses</a>. </h6>

							<h6 class="my-1 text-justify p-3"
								style="border: 1px solid lightsalmon;">

							But, after approving or changing Tickets, <b>click the button below to update the total IP Asset Inventory</b> in the database.</h6>
						

					    <button name="update_inventory_db" 
					    		class="btn btn-sm btn-warning btn-outline-success my-1 ml-3">
							<a 
							   href="{{ route('ip_assets.edit', $ip_asset->id) }}">
								Update IP Asset Inventory Database
							</a>
						</button>
					</div>
				</div>
				{{-- license_limit end --}}

				{{-- inventory start --}}
				<div class="row my-2">
					<div class="col-4 ">Available Licenses In Inventory</div>
					<div class="col">
						<span >
							{{ $ip_asset->inventory }}
						</span>
					</div>
				</div>
				{{-- inventory end --}}


				{{-- quantity_licensed start --}}
				<div class="row my-2 ">
					<div class="col-4 ">Quantity Licensed</div>
					<div class="col">
						<span >
							{{ $ip_asset->quantity_licensed }}
						</span>
					</div>
				</div>
				{{-- quantity_licensed end --}}

				
				
				@endcan

				
				@can('isAdmin')
					{{-- admin buttons start --}}
					<div class="row">
						<div class="col-4">Edit | Delete</div>
						{{-- edit start --}}
						<div class="col col-md-4">
							@include('ip_assets.partials.edit-btn')
						</div>
						{{-- edit end --}}

						{{-- delete start --}}
						<div class="col col-md-4">
							@include('ip_assets.partials.delete-form')
						</div>
						{{-- delete end --}}
					</div>
					{{-- admin buttons end --}}
				@endcan

			</div>


			<div class="col-11 col-md-12 col-lg-3 my-2 mx-auto">
					
					<h5 class="mx-auto text-warning my-2" style="text-shadow: 3px 3px 4px navy;">

							Request From Available Licenses

					</h5>
						@foreach($available_ip_licenses as $available_ip_license)
									
								<div class="row my-2 p-2 w-100">
													<!-- Button trigger modal -->
													<button type="button" 
														class="btn btn-outline-warning btn-success w-100" 
														data-toggle="modal" 
														{{-- style="background-color: #FEEDDB;"  --}}
														data-target="#{{ $available_ip_license->code }}">
													  {{ $available_ip_license->code }}
															
															<br>
														{{ $available_ip_license->duration_in_months }}-month License
													</button>

													<!-- Modal -->
													<div class="modal fade " id="{{ $available_ip_license->code }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
													  <div class="modal-dialog modal-dialog-centered " role="document">
													    <div class="modal-content ">
													      <div class="modal-header bg-success">
													        <h5 class="modal-title" id="exampleModalCenterTitle">{{ $available_ip_license->code }}</h5>
													        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
													          <span aria-hidden="true">&times;</span>
													        </button>
													      </div>
													      <div class="modal-body">
													        
																						<div class="card-body">
																							<h6 class="card-title">
																								{{ $available_ip_license->code }}
																							</h6>
																							<br>
																							<h6>{{ $available_ip_license->duration_in_months }}-month License </h6>

																							<p class="card-text my-1">
																								<span 
																							  			style="font-family: 'Libre Barcode 128', cursive; font-size: 2em; "
																								>
																									{{ $available_ip_license->code }}
																								</span>
																								
																							</p>

																								<span class="badge badge-success">
																									{{ $available_ip_license->ip_license_status->name }} 
																								</span>

																							<a href="{{ route('ip_licenses.show', $available_ip_license->id )}}" class="btn btn-sm btn-info w-100">
																								View
																							</a>
																								@cannot('isAdmin')
												  											@include('ip_assets.partials.request')
												  										@endcannot
																						</div>
																						
																					

																									{{-- ip_license card end --}}
																			      </div>
																			      <div class="modal-footer">
																			        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
																			        
																			      </div>
									    				</div>
									  		</div>
									</div>
								</div>
						@endforeach

				@can('isAdmin')
				{{-- quantity_licensed start --}}
				<div class="row mt-5 mx-auto">

					<h5 class="text-warning text-center"

						style="text-shadow: 3px 3px 3px navy;"

					>Granted License(s) Details</h5>

				</div>
				<div class="row my-2">
					
					<div class="col text-warning">
						<ol>
							@foreach($granted_licenses as $granted_license)

								<li>
									<span 
										  style="font-family: 'Libre Barcode 128', cursive; font-size: 2.5em;"
									>
										 {{ $granted_license->code }} 
									</span> <br>

									<badge class="badge-subtle">

									 License Code: {{ $granted_license->code }}      
										
									</badge> <br>

									{{-- <span class="badge badge-warning">To {{ $approved_tickets->user_id }}</span> --}}


									<badge class="badge-subtle">
										License Duration: {{ $granted_license->duration_in_months }} month(s)
									</badge>
								</li>

							@endforeach
						</ol>
					</div>
				</div>
				{{-- quantity_licensed end --}}
				@endcan

			</div>




		</div>
	</div>

@endsection