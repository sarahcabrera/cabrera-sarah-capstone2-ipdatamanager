@extends('layouts.app')



@section('content')

	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif




	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Add IP Asset
				</h1>
			</div>
		</div>

		{{-- add ip_asset section start --}}
		<div class="row">
			<div class="col-12 col-md-10 col-lg-6 mx-auto">
				
				{{-- add ip_asset form start --}}
				<form 
					action="{{ route('ip_assets.store')}}" 
					method="post"
					enctype="multipart/form-data" 
				>
					@csrf

					 {{-- name --}}
						@include('ip_assets.partials.form-group', [

									'name' => 'name',
									'type' => 'text',
									'classes' => ['form-control', 'form-control-sm']

						])


					{{-- creator --}}
						@include('ip_assets.partials.form-group', [

									'name' => 'creator',
									'type' => 'text',
									'classes' => ['form-control', 'form-control-sm']

						])

					 {{-- ip asset code --}}
								@php
									$randomCode = Str::upper(Str::random(10));
								@endphp
								
									<div class="form-group my-2 w-100">
											<div class="input-group mb-3">
													  <div class="input-group-prepend">
													    <span class="input-group-text" id="inputGroup-sizing-default">

													    	IP Asset Code

																	</span>
													  </div>
													  <input 
																		type="text" 
																		name="code" 
																		id="code" 
																		class=" form-control form-control-default

																			@error('code') 
																				is-invalid 
																			@enderror

																			" 
																		placeholder="{{ $randomCode }}" 
																		value="{{ $randomCode }}"
																		autofocus
																		required
																		aria-label="code" 
																		aria-describedby="inputGroup-sizing-default"
																>
											</div>
									</div>



					 {{-- image --}}
					 @include('ip_assets.partials.form-group', [

									'name' => 'image',
									'type' => 'file',
									'classes' => ['form-control', 'form-control-sm']
									

						])

					 {{-- ip_id --}}
					 <label for="ip_id" class="mt-2">IP Category:</label>
					 <select name="ip_id" id="ip_id" class="form-control form-control-sm">

					 		@foreach($ips as $ip)
					 			<option value="{{ $ip->id }}">
					 				{{ $ip->name }}
					 			</option>
							@endforeach
					 </select>

					 {{-- type --}}
					 <label for="type_id" class="mt-2">IP Type:</label>
					 <select name="type_id" id="type_id" class="form-control form-control-sm">

					 		@foreach($types as $type)
						 			<option value="{{ $type->id }}">
						 				{{ $type->name }}
						 			</option>
					 		@endforeach
					 </select>


					 {{-- status --}}
					 <label for="status_id" class="mt-2">Status:</label>
					 <select name="status_id" id="status_id" class="form-control form-control-sm">

					 		@foreach($statuses as $status)
						 			<option value="{{ $status->id }}">
						 				{{ $status->name }}
						 			</option>
					 		@endforeach
					 </select>

					{{-- license_limit --}}
					 @include('ip_assets.partials.form-group', [

									'name' => 'license_limit',
									'type' => 'number',
									'min' => 0,
									'value' => 1,
									'classes' => ['form-control', 'form-control-sm']

						])

						{{-- quantity_licensed --}}
					 @include('ip_assets.partials.form-group', [

									'name' => 'quantity_licensed',
									'type' => 'number',
									'min' => 0,
									'classes' => ['form-control', 'form-control-sm']

						])


						{{-- inventory --}}
					 @include('ip_assets.partials.form-group', [

									'name' => 'inventory',
									'type' => 'number',
									'min' => 0,
									'value' => 1,
									'classes' => ['form-control', 'form-control-sm']

						])


					{{-- creation_date --}}
					@include('ip_assets.partials.form-group',[
						'name' => 'creation_date',
						'type' => 'date',
						'classes' => ['form-control', 'form-control-sm', 'required']
						
					])

					{{-- registered_date --}}
					@include('ip_assets.partials.form-group',[
						'name' => 'registered_date',
						'type' => 'date',
						'classes' => ['form-control', 'form-control-sm', 'required']
						
					])

					{{-- renewal_due_date --}}
					@include('ip_assets.partials.form-group',[
						'name' => 'renewal_due_date',
						'type' => 'date',
						'classes' => ['form-control', 'form-control-sm', 'required']
						
					])


					{{-- renewal_status --}}
					 <label for="renewal_status_id" class="mt-2">Renewal Status:</label>
					 <select name="renewal_status_id" id="renewal_status_id" class="form-control form-control-sm">

					 		@foreach($renewal_statuses as $renewal_status)
						 			<option value="{{ $renewal_status->id }}">
						 				{{ $renewal_status->name }}
						 			</option>
					 		@endforeach
					 </select>




					 {{-- description --}}
					 <label for="description" class="mt-3">IP Asset description:</label>
					 <textarea 
					 			name="description" 
					 			id="description" 
					 			cols="30" 
					 			rows="10" 
					 			class="form-control form-control-sm @error('description') is-invalid	@enderror"
					 			></textarea>

					 @error('description')
						<small class="d-block invalid-feedback">
							<strong>
								{{ $message }}
							</strong>
						</small>
					@enderror

					 <button class="btn btn-sm btn-primary mt-3">Add IP Asset</button>

					
				</form>
				{{-- add ip_asset form end --}}
			</div>
		</div>
		{{-- add ip_asset section end --}}
	</div>



@endsection