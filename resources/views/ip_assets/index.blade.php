@extends('layouts.app')

@section('content')

	{{-- alert-message --}}
	@includeWhen(Session::has('message'),'partials.alert')
	
	<div class="container container-fluid">
		{{-- header start --}}
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					IP Assets Catalog
				</h1>
			</div>
		</div>
		{{-- header end --}}

		{{-- ip_assets section start --}}
		<div class="row">
			@foreach($ip_assets as $ip_asset)
				<div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-3 mx-auto">
					{{-- ip_asset card start --}}
					<div class="card">
						<a href="{{ route('ip_assets.show', $ip_asset->id) }}">
							<img src="{{ $ip_asset->image }}" alt="" class="card-img-top" width="200" height="200">
						</a>
						
						<div class="card-body">
							<h5 class="card-title">
								{{ $ip_asset->name }}
							</h5>
							<span class="badge badge-light p-3">
								AVAILABLE LICENSES: ( {{ $ip_asset->inventory }} / {{ $ip_asset->license_limit }} ) 
							</span>

							<p class="card-text my-1">
								<span 
							  			style="font-family: 'Libre Barcode 128', cursive; font-size: 3em; "
								>
									{{ $ip_asset->code }}
								</span>
								<span class="badge badge-light p-3">
									{{ $ip_asset->code }}
								</span>
							</p>

							<p class="card-text my-1">
								
								<span class="badge 
											badge-{{ $ip_asset->ip->id == 1 ? "warning" : ($ip_asset->ip->id == 2 ? "info" : "danger" ) }}
											">
									{{ $ip_asset->ip->name }}
								</span>

								<span class="badge badge-success">
									{{ $ip_asset->type->name }}
								</span>

								<span class="badge badge-light">
									{{ $ip_asset->status->name }}
								</span>
							</p>

							{{-- @include('ip_assets.partials.request') --}}




							<a href="{{ route('ip_assets.show', $ip_asset->id) }}" class="btn btn-sm btn-info w-100">
								View
							</a>
						</div>
						@can('isAdmin')
						<div class="card-footer">

							@include('ip_assets.partials.edit-btn')
							@include('ip_assets.partials.delete-form')
						
						</div>
						@endcan
						
					</div>

					{{-- ip_asset card end --}}
				</div>
			@endforeach
		</div>

		{{-- ip_assets section end --}}
	</div>
@endsection