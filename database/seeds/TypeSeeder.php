<?php

use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('types')->insert([

            'name' => 'IT Training'

        ]);

         DB::table('types')->insert([

            'name' => 'Vehicle'

        ]);

        DB::table('types')->insert([

            'name' => 'Administrative Services'

        ]);


        DB::table('types')->insert([

        	'name' => 'Art'

        ]);

        DB::table('types')->insert([

        	'name' => 'Articles'

        ]);

        DB::table('types')->insert([

        	'name' => 'Books'

        ]);

        DB::table('types')->insert([

        	'name' => 'Movies'

        ]);

        DB::table('types')->insert([

        	'name' => 'Photographs'

        ]);

        DB::table('types')->insert([

        	'name' => 'Videos'

        ]);


        DB::table('types')->insert([

        	'name' => 'Electronics'

        ]);


        DB::table('types')->insert([

        	'name' => 'Appliances'

        ]);


        DB::table('types')->insert([

        	'name' => 'Robots'

        ]);


        DB::table('types')->insert([

        	'name' => 'Computers'

        ]);


        DB::table('types')->insert([

        	'name' => 'Apparel'

        ]);


        DB::table('types')->insert([

        	'name' => 'Footwear'

        ]);


        DB::table('types')->insert([

        	'name' => 'Eyewear'

        ]);


        
    }
}
