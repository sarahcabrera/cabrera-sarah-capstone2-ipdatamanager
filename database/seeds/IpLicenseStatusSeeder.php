<?php

use Illuminate\Database\Seeder;

class IpLicenseStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ip_license_statuses')->insert([

            'name' => "Available"
        ]);


        DB::table('ip_license_statuses')->insert([

            'name' => "Unavailable"
        ]);

    }
}
