<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([

        	'name' => 'Available'

        ]);

        DB::table('statuses')->insert([

        	'name' => 'In Production'

        ]);

        DB::table('statuses')->insert([

        	'name' => 'Unavailable: Out of Stock'

        ]);

        DB::table('statuses')->insert([

        	'name' => 'Unavailable: Registration Lapsed'

        ]);
    }
}
