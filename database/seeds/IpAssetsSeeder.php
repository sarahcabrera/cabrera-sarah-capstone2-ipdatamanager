<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class IpAssetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('ip_assets')->insert([
        	'name' => 'Women Who Run With the Wolves',
            'code' => Str::upper(Str::random(10)),
            'creator' => 'Dr. Clarissa Pinkola-Estes',
        	'description' => "Women Who Run with the Wolves: Myths and Stories of the Wild Woman Archetype is a book by Jungian analyst, author and poet Clarissa Pinkola Estés, Ph.D, published in 1992 by Ballantine Books. It spent 145 weeks on The New York Times Best Seller list over a three-year span, a record at the time. Estés won a Las Primeras Award from the Mexican American Women's Foundation for being the First Latina on the New York Times Best Seller list.[2] The book also appeared on other best seller lists, including USA Today, Publishers Weekly, and Library Journal.

				In Women Who Run with the Wolves, Estés analyses myths, fairy tales, folk tales and stories from different cultures to uncover the Wild Woman archetype of the feminine psyche. The book stems from these interpretations of old tales and creates this wolf-woman parallel, by incorporating her own previous studies that suggest wolves and women are relational by nature. The notion of the archetype is associated with the work of Carl Jung. Estés produces this new collection of words to describe the female psyche in Women who Run with the Wolves as it is a psychology of women in the truest sense, a knowing of the soul.",

        	'image' => 'https://ipdatamanager.s3.amazonaws.com/images/Women_Who_Run_with_the_Wolves.jpg',
            'path' => 'https://ipdatamanager.s3.amazonaws.com/images/Women_Who_Run_with_the_Wolves.jpg',
        

        	'ip_id' => 1,
        	'status_id' => 1,
        	'type_id' => 6,
            'license_limit' => 4,
            'quantity_licensed' => 2,
            'inventory' => 2,
            'renewal_status_id' => 5,

        	'creation_date' => "1992-01-12",
        	'registered_date' => "1992-02-12",
            'renewal_due_date' => "2100-01-01"

        ]);

         DB::table('ip_assets')->insert([
        	'name' => 'Dune (2020 Movie)',
            'code' => Str::upper(Str::random(10)),
            'creator' => 'Dennis Villeneuve',
        	'description' => "A world beyond your experience, beyond your imagination. A place beyond your dreams. A movie beyond your imagination.

						        	Dune is an upcoming 2020 epic science fiction film directed by Denis Villeneuve with a screenplay by Jon Spaihts, Eric Roth and Villeneuve. The film is an international co-production of Canada, Hungary, the United Kingdom and the United States, and it is the first of a planned two-part adaptation of the 1965 novel of the same name by Frank Herbert, which will cover roughly the first half of the book.

						The film stars an ensemble cast including Timothée Chalamet, Rebecca Ferguson, Oscar Isaac, Josh Brolin, Stellan Skarsgård, Dave Bautista, Stephen McKinley Henderson, Zendaya, David Dastmalchian, Chang Chen, Sharon Duncan-Brewster, Charlotte Rampling, Jason Momoa and Javier Bardem.",

        	'image' => 'https://ipdatamanager.s3.amazonaws.com/images/Dune-Movie-Poster-Haley-Turnbull.jpg',
            'path' => 'https://ipdatamanager.s3.amazonaws.com/images/Dune-Movie-Poster-Haley-Turnbull.jpg',

        	
            'ip_id' => 1,
            'status_id' => 2,
            'type_id' => 7,
            'license_limit' => 7,
            'quantity_licensed' => 3,
            'inventory' => 4,
            'renewal_status_id' => 5,

        	'creation_date' => "2020-08-12",
            'registered_date' => "2020-08-16",
            'renewal_due_date' => "2100-01-01"

        ]);

         DB::table('ip_assets')->insert([
        	'name' => 'Cinera Edge',
            'code' => Str::upper(Str::random(10)),
            'creator' => 'Cinere, Inc.',
        	'description' => "Cinera Edge is the world's first personal cinema HMD that features dual 2.5K micro-OLED and a Dolby Digital® certified headphone with 5.1 channels of surround sound. The combination boasts a real theatrical immersive experience with fantastic video and audio quality, making the device a true mobile cinema. It invigorates your movie, TV, or gaming experience, and escalates your daily entertainment to a new level.",

        	'image' => 'https://ipdatamanager.s3.amazonaws.com/images/Cinera-Edge-1.jpg',
            'path' => 'https://ipdatamanager.s3.amazonaws.com/images/Cinera-Edge-1.jpg',


            'ip_id' => 3,
            'status_id' => 1,
            'type_id' => 10,
            'license_limit' => 5,
            'quantity_licensed' => 1,
            'inventory' => 4,
            'renewal_status_id' => 1,
            'creation_date' => "2020-01-12",
            'registered_date' => "2020-02-12",
            'renewal_due_date' => "2030-01-01"

        ]);


         DB::table('ip_assets')->insert([
            'name' => 'Zuitt',
            'code' => Str::upper(Str::random(10)),
            'creator' => 'Tuitt Philippines, Inc.',
            'description' => "Zuitt is the #1 Philippine-based startup offering web development coding bootcamps in Manila. Zuitt has helps over 1,000 Filipinos learn web development every year and equip them with the job hunting skills needed to get hired as Software Engineers. Their secret to success is their belief that Filipinos can do much more with affordable and quality education. If you invest in your education, you can be in the best position to achieve the life you’ve always wanted for yourself and your family.",

            'image' => 'https://ipdatamanager.s3.amazonaws.com/images/zuittlogo.png',
            'path' => 'https://ipdatamanager.s3.amazonaws.com/images/zuittlogo.png',
            
            
            'ip_id' => 2,
            'status_id' => 1,
            'type_id' => 1,
            'license_limit' => 3,
            'quantity_licensed' => 0,
            'inventory' => 3,
            'renewal_status_id' => 2,

            'creation_date' => "2020-01-12",
            'registered_date' => "2020-02-12",
            'renewal_due_date' => "2025-01-01"
    

        ]);


           DB::table('ip_assets')->insert([
            'name' => 'Pero Wearable Mouse',
            'code' => Str::upper(Str::random(10)),
            'creator' => 'Palmcat Corp.',
            'description' => "Pero: Wearable Mouse That Also Executes Your Gesture Command
            Pero can be used as an ordinary mouse, game controller, or as a control device for design programs and Microsoft Office Suite programs! Introducing Pero, a wearable controller that makes your smart devices even smarter. Pero turns your simple hand motions into customizable shortcuts for your device. Now, you can control the cursor and input commands with this single device so that you can focus more on getting the job done. ",

            'image' => 'https://ipdatamanager.s3.amazonaws.com/images/pero_wearable_mouse_with_gesture_control_1.jpg',
            'path' => 'https://ipdatamanager.s3.amazonaws.com/images/pero_wearable_mouse_with_gesture_control_1.jpg',
            

            'ip_id' => 3,
            'status_id' => 1,
            'type_id' => 10,
            'license_limit' => 5,
            'quantity_licensed' => 2,
            'inventory' => 3,
            'renewal_status_id' => 1,


            'creation_date' => "2020-01-12",
            'registered_date' => "2020-02-12",
            'renewal_due_date' => "2030-01-01"
            

        ]);

        DB::table('ip_assets')->insert([
            'name' => 'RareJob',
            'code' => Str::upper(Str::random(10)),
            'creator' => 'RareJob Philippines, Inc.',
            'description' => "RAREJOB CHANCES FOR EVERYONE, EVERYWHERE",

            'image' => 'https://ipdatamanager.s3.amazonaws.com/images/RareJob.jpg',
            'path' => 'https://ipdatamanager.s3.amazonaws.com/images/RareJob.jpg',
            

            'ip_id' => 2,
            'status_id' => 1,
            'type_id' => 3,
            'license_limit' => 5,
            'quantity_licensed' => 2,
            'inventory' => 3,
            'renewal_status_id' => 2,


            'creation_date' => "2020-01-12",
            'registered_date' => "2020-02-12",
            'renewal_due_date' => "2025-01-01"
            

         ]);

    }
}
