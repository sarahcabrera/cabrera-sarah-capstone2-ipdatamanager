<?php

use Illuminate\Database\Seeder;

class RenewalStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('renewal_statuses')->insert([

        	'name' => 'New Registration'

        ]);

        DB::table('renewal_statuses')->insert([

        	'name' => 'Renewed Registration'

        ]);


        DB::table('renewal_statuses')->insert([

        	'name' => 'Renewal Due Date Soon'

        ]);


        DB::table('renewal_statuses')->insert([

        	'name' => 'Renewal Due Date Lapsed'

        ]);

        DB::table('renewal_statuses')->insert([

        	'name' => 'Registered Copyright'

        ]);

        DB::table('renewal_statuses')->insert([

        	'name' => 'Copyright Expired'

        ]);
    }
}
