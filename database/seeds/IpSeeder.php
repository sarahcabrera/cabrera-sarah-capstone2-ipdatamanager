<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ips')->insert([

        	'name' => 'Copyrights',
        	'term' => 'Lifetime of author plus 50 years',
        	'protection_start' => 'Date of creation',
        	'description' => 'The creators of works protected by copyright hold the exclusive right to use or 		authorize others to use the work on agreed terms.

						The right holder(s) of a work can authorize or prohibit: its reproduction in all forms, including print form and sound recording, public performance and communication to the public, broadcasting, translation into other languages, and  adaptation, such as from a novel to a screenplay for a film.',
            'works_covered' => 'Works covered by copyright are, but are not limited to: novels, poems, plays, reference works, newspapers, advertisements, computer programs, databases, films, musical compositions, choreography, paintings, drawings, photographs, sculpture, architecture, maps and technical drawings.'

        ]);

        DB::table('ips')->insert([

        	'name' => 'Trademarks',
        	'term' => '10 years from the date of issuance and is renewable for a period of 10 years at a time. A trademark can be protected in perpetuity if regularly monitored and properly maintained.',
        	'protection_start' => 'Date of registration',
        	'description' => 'A trademark protects a business’ brand identity in the marketplace.

							Registration of it gives the owner the exclusive rights to prevent others from using or exploiting the mark in any way.

							Aside from being a source-identifier, differentiator, quality indicator, and an advertising device, a protective mark may also bring another stream of income to the owner through licensing or franchising. ',
            'works_covered' => 'Distinctive trademarks and wordmarks.'

        ]);

        DB::table('ips')->insert([

        	'name' => 'Patents',
        	'term' => '20 years from the filing date of the application. The patent must be maintained yearly, starting from the 5th year.',
        	'protection_start' => 'Date of filing of application',
        	'description' => 'A patent is an exclusive right that allows the inventor to exclude others from making, using, or selling the product of his invention during the life of the patent. Patent owners may also give permission to, or license, other parties to use their inventions on mutually agreed terms. Owners may also sell their invention rights to someone else, who then becomes the new owner of the patent.',
            'works_covered' => 'The Intellectual Property Code of the Philippines sets three conditions for an invention to be deemed patentable: it has to be new, involves an inventive step, and industrially applicable.

 

            How are these defined? In the IP Code, an invention is not considered new if it already forms part of the domain of prior art.  Prior art is explained in the Intellectual Property Code of the Philippines, Chapter 2, Section 24 - 24.2

 

            An invention involves an inventive step if, having regard to prior art, it is not obvious to a person skilled in the art at the time of the filing date or priority date of the application claiming the invention. An invention that can be produced and used in any industry is considered industrially applicable.'

        ]);

         DB::table('ips')->insert([

            'name' => 'Utility Models',
            'term' => '7 years of protection from the date of filing, with no possibility of renewal.',
            'protection_start' => 'Date of filing of application',
            'description' => 'A Utility Model (UM) allows the right holder to prevent others from commercially using the registered UM without his authorization, provided that the UM is new based on the Registrability Report. Compared with invention patents, it is relatively inexpensive, faster to obtain, and with less stringent patentability requirements.',
            'works_covered' => 'Any technical solution of a problem in any field of human activity which is new and industrially applicable shall be registrable.

                The provisions regarding “Non-Patentable Inventions” as provided for in Part 2, Rule 202 of the Regulations for Patents shall apply, mutatis mutandis, to non-registrable utility models:


                (1.) Discoveries, scientific theories and mathematical methods;


                (2.) Schemes, rules and methods of performing mental acts, playing games or doing business, and programs for computers;


                (3.) Methods for treatment of the human or animal body by surgery or therapy and diagnostic methods practiced on the human or animal body. This provision shall not apply to products and composition for use in any of these methods;


                (4.) Plant varieties or animal breeds or essentially biological process for the production of plants or animals. This provision shall not apply to micro-organisms and non-biological and microbiological processes.


                (5.) Provisions under this subsection shall not preclude Congress to consider the enactment of a law providing sui generis protection of plant varieties and animal breeds and a system of community intellectual rights protection:

                (6.) Aesthetic creations; and

                (7.) Anything which is contrary to public order or morality.'

        ]);

         DB::table('ips')->insert([

            'name' => 'Industrial Design',
            'term' => '5 years from the filing date of the application. The registration may be renewed for not more than 2 consecutive periods of 5 years. The renewal fee should be paid within a year of the expiration of the registration.',
            'protection_start' => 'Date of filing of application',
            'description' => 'The owner of a registered industrial design has the right to prevent third parties from making, selling or importing articles bearing or embodying a design which is a copy, or substantially a copy, of the protected design, when such acts are undertaken for commercial purposes.',
            'works_covered' => 'In order to be registrable, an industrial design must be a new or original creation. 

                The following industrial designs shall not be registrable:

                (a) Industrial designs that are dictated essentially by technical or functional considerations to obtain a technical result;

                (b) Industrial designs which are mere schemes of surface ornamentations existing separately from the industrial product or handicraft; and

                (c) Industrial designs which are contrary to public order, health, or morals.'

        ]);
    }
}
