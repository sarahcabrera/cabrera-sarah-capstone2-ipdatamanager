<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class IpLicenseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Asset 1: Women Who Run With the Wolves
    	DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 1,
            'duration_in_months' => 24,
            'ip_license_status_id' => 2
        ]);

        DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 1,
            'duration_in_months' => 12,
            'ip_license_status_id' => 1
        ]);

        DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 1,
            'duration_in_months' => 12,
            'ip_license_status_id' => 2
        ]);

         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 1,
            'duration_in_months' => 12,
            'ip_license_status_id' => 1
        ]);


         //Asset 2: Dune
         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 2,
            'duration_in_months' => 1,
            'ip_license_status_id' => 2
        ]);


         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 2,
            'duration_in_months' => 1,
            'ip_license_status_id' => 2
        ]);


         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 2,
            'duration_in_months' => 1,
            'ip_license_status_id' => 2
        ]);


         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 2,
            'duration_in_months' => 1,
            'ip_license_status_id' => 1
        ]);


         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 2,
            'duration_in_months' => 1,
            'ip_license_status_id' => 1
        ]);

         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 2,
            'duration_in_months' => 1,
            'ip_license_status_id' => 1
        ]);


         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 2,
            'duration_in_months' => 1,
            'ip_license_status_id' => 1
        ]);


         //Asset 3: Cinera Edge
         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 3,
            'duration_in_months' => 24,
            'ip_license_status_id' => 2
        ]);


         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 3,
            'duration_in_months' => 12,
            'ip_license_status_id' => 1
        ]);


         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 3,
            'duration_in_months' => 12,
            'ip_license_status_id' => 1
        ]);


         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 3,
            'duration_in_months' => 12,
            'ip_license_status_id' => 1
        ]);


         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 3,
            'duration_in_months' => 12,
            'ip_license_status_id' => 1
        ]);


         //Asset 4: Zuitt
         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 4,
            'duration_in_months' => 12,
            'ip_license_status_id' => 1
        ]);

         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 4,
            'duration_in_months' => 12,
            'ip_license_status_id' => 1
        ]);


         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 4,
            'duration_in_months' => 12,
            'ip_license_status_id' => 1
        ]);


         //Asset 5: Pero Wearable Mouse
         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 5,
            'duration_in_months' => 36,
            'ip_license_status_id' => 2
        ]);

         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 5,
            'duration_in_months' => 24,
            'ip_license_status_id' => 2
        ]);

         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 5,
            'duration_in_months' => 12,
            'ip_license_status_id' => 1
        ]);

         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 5,
            'duration_in_months' => 12,
            'ip_license_status_id' => 1
        ]);

         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 5,
            'duration_in_months' => 12,
            'ip_license_status_id' => 1
        ]);


         //Asset 6: RareJob
         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 6,
            'duration_in_months' => 24,
            'ip_license_status_id' => 2
        ]);

         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 6,
            'duration_in_months' => 12,
            'ip_license_status_id' => 2
        ]);

         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 6,
            'duration_in_months' => 12,
            'ip_license_status_id' => 1
        ]);

         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 6,
            'duration_in_months' => 12,
            'ip_license_status_id' => 1
        ]);

         DB::table('ip_licenses')->insert([

            'code' => Str::upper(Str::random(16)),
            'ip_asset_id' => 6,
            'duration_in_months' => 12,
            'ip_license_status_id' => 1
        ]);

    }
}
