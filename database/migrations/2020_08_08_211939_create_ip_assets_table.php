<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;


class CreateIpAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ip_assets', function (Blueprint $table) {

            $table->id();
            $table->timestamps();
            $table->string('code')->default(Str::upper(Str::random(10)));
            $table->string('name')->unique();
            $table->text('description');
            $table->string('creator');

            $table->string('image');
            $table->string('path')->nullable();
            $table->double('size', 8, 2)->default(0);
            $table->unsignedInteger('auth_by')->nullable();

            
            $table->bigInteger('license_limit');
            $table->bigInteger('quantity_licensed')->nullable();
            $table->bigInteger('inventory')->nullable();
            $table->foreignID('ip_id')->constrained('ips');
            $table->foreignID('status_id')->constrained('statuses');
            $table->foreignID('type_id')->constrained('types');
            $table->foreignID('renewal_status_id')->default(1)->constrained('renewal_statuses');
            $table->date('creation_date')->nullable();
            $table->date('registered_date')->nullable();
            $table->date('renewal_due_date')->nullable();
            $table->softDeletes();
            
            
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ip_assets');
    }
}
