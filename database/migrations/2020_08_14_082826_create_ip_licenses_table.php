<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIpLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ip_licenses', function (Blueprint $table) {
            $table->id();
            $table->string('code')->default(Str::upper(Str::random(16)));
            $table->foreignID('ip_asset_id')->constrained('ip_assets');
            $table->integer('duration_in_months')->default(12);
            $table->foreignID('ip_license_status_id')->default(1)->constrained('ip_license_statuses');
            $table->date('date_needed')->nullable();
            $table->date('date_returned')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ip_licenses');
    }
}
