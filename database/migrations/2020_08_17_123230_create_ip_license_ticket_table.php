<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIpLicenseTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ip_license_ticket', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignID('ticket_id')->constrained('tickets');
            $table->foreignID('ip_license_id')->constrained('ip_licenses');
            $table->integer('duration_in_months');
            $table->date('date_needed')->nullable();
            $table->date('return_date')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ip_license_ticket');
    }
}
